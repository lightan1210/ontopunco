<?php
/*

	 Copyright 2016 Giménez, Christian. Germán Braun.

	 Author: Giménez, Christian

	 wicomtest.php

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

// use function \load;
load("wicom.php", "common/");
load("uml.php", "common/");
load("config.php", "config/");

use Wicom\Wicom;
use Wicom\UML_Wicom;

class AutomappingTest extends PHPUnit\Framework\TestCase
{
		public function testautomapping(){
				$input = '{"namespaces":{"ontologyIRI":[{"prefix":"crowd","value":"http://crowd.fi.uncoma.edu.ar/"}],"defaultIRIs":[{"prefix":"rdf","value":"http://www.w3.org/1999/02/22-rdf-syntax-ns#"},{"prefix":"rdfs","value":"http://www.w3.org/2000/01/rdf-schema#"},{"prefix":"xsd","value":"http://www.w3.org/2001/XMLSchema#"},{"prefix":"owl","value":"http://www.w3.org/2002/07/owl#"}],"IRIs":[{"prefix":"peliculas","value":"http://www.semanticweb.org/peliculas#"}]},"classes":[{"name":"http://www.semanticweb.org/peliculas#Actor","attrs":[{"name":"http://www.semanticweb.org/peliculas#ID","datatype":"http://www.w3.org/2001/XMLSchema#integer"},{"name":"http://www.semanticweb.org/peliculas#Name","datatype":"http://www.w3.org/2001/XMLSchema#string"}],"methods":[],"position":{"x":161,"y":119}}],"links":[]}';
				$expected = <<<EOT

@prefix rr: <http://www.w3.org/ns/r2rml#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix : <http://www.benchmark.com/quest/> .
@base <http://example.com/base/> .



###########################################
# TripleMap for 0: http://www.semanticweb.org/peliculas#Actor
<mapping1_0> a rr:TriplesMap;
	rr:logicalTable [ rr:sqlQuery "SELECT \"Actor\".\"ID\" AS \"actorid\", \"Actor\".\"Name\" AS \"actorname\" FROM public.\"Actor\"" ];
	rr:subjectMap [	rr:template "http://www.automated-examples.com/resource/actor/{\"actorid\"}";
		rr:class <http://www.semanticweb.org/peliculas#Actor>
	];

	rr:predicateObjectMap [
		rr:predicate 	<http://www.semanticweb.org/peliculas#ID> ;
		rr:objectMap [ rr:column "actorid" ]
  	];

	rr:predicateObjectMap [
		rr:predicate 	<http://www.semanticweb.org/peliculas#Name> ;
		rr:objectMap [ rr:column "actorname" ]
  	];

.


EOT;


				$wicom = new UML_Wicom();
				// $GLOBALS['config']['temporal_path'] = "../../temp/";
				//var_dump($input);
				$answer = $wicom->automapping($input);

				/*print("\n\$answer = ");
				print_r($answer);
				print("\n");*/

				/*$answer->set_reasoner_input("");
				$answer->set_reasoner_output("");
				$actual = $answer->to_json();*/

				//var_dump($answer);

				$this->assertEquals(trim($expected),trim($answer));

		}



}
