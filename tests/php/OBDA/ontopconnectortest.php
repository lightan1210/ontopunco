<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   reasonertest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//require_once("common.php");

// use function \load;

require_once("common.php");

// use function \load;
load("config.php", "config/");
load("ontopconnector.php", "wicom/reasoner/");

//use Wicom\Ontop\OntopConnector;

use Wicom\Reasoner\OntopConnector;

class OntopConnectorTest extends PHPUnit\Framework\TestCase

{

	public function testReasoner(){
		$input = <<<'EOT'
<?xml version="1.0"?>
<Ontology xmlns="http://www.w3.org/2002/07/owl#"
	 xml:base="http://www.semanticweb.org/peliculas"
	 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	 xmlns:xml="http://www.w3.org/XML/1998/namespace"
	 xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
	 xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
	 ontologyIRI="http://www.semanticweb.org/peliculas">
	<Prefix name="" IRI="http://www.semanticweb.org/peliculas#"/>
	<Prefix name="owl" IRI="http://www.w3.org/2002/07/owl#"/>
	<Prefix name="rdf" IRI="http://www.w3.org/1999/02/22-rdf-syntax-ns#"/>
	<Prefix name="xml" IRI="http://www.w3.org/XML/1998/namespace"/>
	<Prefix name="xsd" IRI="http://www.w3.org/2001/XMLSchema#"/>
	<Prefix name="rdfs" IRI="http://www.w3.org/2000/01/rdf-schema#"/>
	<Declaration>
		<DataProperty IRI="#ID"/>
	</Declaration>
	<Declaration>
		<Class IRI="#Actor"/>
	</Declaration>
	<Declaration>
		<DataProperty IRI="#name"/>
	</Declaration>
	<SubDataPropertyOf>
		<DataProperty IRI="#ID"/>
		<DataProperty abbreviatedIRI="owl:topDataProperty"/>
	</SubDataPropertyOf>
	<SubDataPropertyOf>
		<DataProperty IRI="#name"/>
		<DataProperty abbreviatedIRI="owl:topDataProperty"/>
	</SubDataPropertyOf>
	<DataPropertyDomain>
		<DataProperty IRI="#ID"/>
		<Class IRI="#Actor"/>
	</DataPropertyDomain>
	<DataPropertyDomain>
		<DataProperty IRI="#name"/>
		<Class IRI="#Actor"/>
	</DataPropertyDomain>
	<DataPropertyRange>
		<DataProperty IRI="#ID"/>
		<Datatype abbreviatedIRI="xsd:integer"/>
	</DataPropertyRange>
	<DataPropertyRange>
		<DataProperty IRI="#name"/>
		<Datatype abbreviatedIRI="xsd:string"/>
	</DataPropertyRange>
</Ontology>

EOT;

		//Expected obtained directly through racer -- -owllink owllinkfile.owllink
		$expected = <<<'EOT'
n
"Pepe"^^xsd:string
"Juan"^^xsd:string

EOT;

		$ontop = new OntopConnector();

		$ontop->run($input);
		$actual = $ontop->get_col_answers()[0];

		//$expected = process_xmlspaces($expected);
		//$actual = process_xmlspaces($actual);
		$this->assertEquals(trim($expected), trim($actual));
	}
}
?>