<?php
/*

   Copyright 2018 Giménez, Christian

   Author: Giménez, Christian

   placeholders.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div id="translation_placeholder"></div>
<div id="reasoning_placeholder"></div>
<div id="clearwidget_placeholder"></div>
<div id="OBDA_widget_placeholder"></div>

<div id="loginwidget_placer"></div>
<div id="saveloadjson_placer"></div>

<div id="errorwidget_placer"></div>
<div id="owllink_placer"></div>
<div id="importjsonwidget_placer"></div>
<div id="exportjson_placer"></div>

<div id="donewidget_placeholder"></div>

<div id="namespaces_placeholder"></div>

<!-- UML -->
<div id="editclass_placeholder"></div>
<div id="classoptions_placeholder"></div>
<div id="uml_editattr_placer"></div>
<div id="attroptions_placeholder"></div>
<div id="relationoptions_placeholder"></div>
<div id="isaoptions_placeholder"></div>
<div id="metamodel_placeholder"></div>
<div id="uml_translate_placeholder"></div>
<div id="uml_reasoning_placeholder"></div>
<div id="uml_importowlwidget_placer"></div>

<div id="uml_assoctype_placeholder"></div>
<div id="uml_naryoptions_placeholder"></div>
<div id="uml_binaryoptions_placeholder"></div>

<div id="associconoptions"></div>
<div id="generalisationiconoptions"></div>
<div id="editassoc"></div>
<div id="infowidget_placer"></div>

<!-- EER -->
<div id="eercreateentity_placeholder"></div>
<div id="eereditentity_placeholder"></div>
<div id="eerentityoptions_placeholder"></div>
<div id="eerrelationoptions_placeholder"></div>
<div id="eerisaoptions_placeholder"></div>
<div id="eerattraptions_placeholder"></div>
