# guiorm.coffee --
# Copyright (C) 2016

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.gui = exports.gui ? {}


# @namespace gui
#
# Central GUI *do-it-all* class...
#
class GUIORM extends gui.GUIIMPL
    constructor: (@graph,@paper) ->
        super()
        @urlprefix = ""

        @diag = new model.orm.ORMDiagram(@graph)
        @diagadapter = new gui.ORMAdapter(@diag, @paper)

        @state = gui.state_inst.selection_state()

        @widgets = new gui.ORMWidgets()
            
        @serverconn = new ServerConnection( (jqXHR, status, text) ->
            exports.gui.gui_instance.widgets.show_error(
                status + ": " + text , jqXHR.responseText)
        )

        gui.set_current_instance(this);

    set_urlprefix: (str) -> @urlprefix = str

    # What to do when the user clicked on a cellView.
    on_cell_clicked: (cellview, event, x, y) ->
        @state.on_cell_clicked(cellview, event, x, y, this)

    set_editclass_classid: (model_id) ->
        # editclass = new EditClassView({el: $("#editclass")})
        @editclass.set_classid(model_id)


    # Update the interface with satisfiable information.
    #
    # @param data {string} is a JSON string with the server response.
    update_satisfiable: (data) ->
        console.log(data)
        obj = JSON.parse(data);

        this.set_trafficlight(obj)
        $("#reasoner_input").html(obj.reasoner.input)
        $("#reasoner_output").html(obj.reasoner.output)
        $.mobile.loading("hide")
        @diagadapter.set_unsatisfiable(obj.unsatisfiable.classes)
        @diagadapter.set_satisfiable(obj.satisfiable.classes)
        # this.change_to_details_page()

    # Set the traffic-light according to the JSON object recived by the server.
    #
    # @param obj {JSON} The JSON object parsed from the recieved data.
    set_trafficlight: (obj) ->
        if (obj.satisfiable.kb)
            if (obj.unsatisfiable.classes.length == 0)
                @trafficlight.turn_green()
            else
                @trafficlight.turn_yellow()
        else
            @trafficlight.turn_red()

    #
    # Send a POST to the server for checking if the diagram is
    # satisfiable.
    check_satisfiable: () ->
        $.mobile.loading("show",
            text: "Consulting server...",
            textVisible: true,
            textonly: false
        )
        @serverconn.request_satisfiable(
            gui.gui_instance.current_gui.diagadapter.diag_to_json(),
            gui.gui_instance.update_satisfiable # Be careful with the context
            # change! this will have another object...
            )

    # Update the translation information on the GUI and show it to the
    # user.
    #
    # Depending on the format selected by the user show it as HTML or
    # inside a textarea tag.
    #
    # @param data {string} The HTML, OWLlink or the translation
    # string.
    # @see CreateClassView#get_translation_format
    update_translation: (data) ->
        console.log(data)
        format = @crearclase.get_translation_format()
        if format == "html"
            $("#html-output").html(data)
            $("#html-output").show()
            $("#owllink_source").hide()
        else
            $("#owllink_source").text(data)
            $("#owllink_source").show()
            $("#html-output").hide()
        $.mobile.loading("hide")
        gui.gui_instance.change_to_details_page()


    update_metamodel: (data) ->
        console.log(data)
        $("#owllink_source").text(data)
        $("#owllink_source").show()
        $("#html-output").hide()
        $.mobile.loading("hide")
        change_to_details_page()




    ##
    # Event handler for translate diagram to OWLlink using Ajax
    # and the api/translate/berardi.php translator URL.
    translate_owllink: () ->
        format = @crearclase.get_translation_format()
        $.mobile.loading("show",
            text: "Consulting server...",
            textVisible: true,
            textonly: false
        )
        json = @diag.to_json()
        @serverconn.request_translation(JSON.stringify(json), format, gui.update_translation)


    change_to_details_page: () ->
        $.mobile.changePage("#details-page", transition: "slide")

    change_to_diagram_page: () ->
        $.mobile.changePage("#diagram-page", transition: "slide", reverse: true)

    #
    # Hide the left side "Tools" toolbar
    #
    hide_toolbar: () ->
        $("#tools-panel [data-rel=close]").click()


    hide_umldiagram_page: () -> $("#diagram-page").css("display","none")

    show_umldiagram_page: () -> $("#diagram-page").css("display","block")


    # Change the interface into a "new association" state.
    #
    # @param class_id {string} The id of the class that triggered it and thus,
    #   the starting class of the association.
    # @param mult {array} An array of two strings representing the cardinality from and to.
    set_association_state: (class_id, mult) ->
        @state = gui.state_inst.association_state()
        @state.set_cellStarter(class_id)
        @state.set_cardinality(mult)

    # Change to the IsA GUI State so the user can select the child for the parent.
    #
    # @param class_id {String} The JointJS::Cell id for the parent class.
    # @param disjoint {Boolean} optional. If the relation has the disjoint constraint.
    # @param covering {Boolean} optional. If the relation has the disjoint constraint.
    set_isa_state: (class_id, disjoint=false, covering=false) ->
        @state = gui.state_inst.isa_state()
        @state.set_cellStarter(class_id)
        @state.set_constraint(disjoint, covering)

    # Change the interface into a "selection" state.
    set_selection_state: () ->
        @state = gui.state_inst.selection_state()

    ##
    # Show the "Insert OWLlink" section.
    show_insert_owllink: () ->
        this.change_to_details_page()


    to_metamodel: () ->
        $.mobile.loading("show", text: "Metamodelling...", textVisible: true, textonly: false)
        json = JSON.stringify(@diag.to_json())
        @serverconn.request_metamodel_translation(json,this.update_metamodel)

    to_erd: (gui_instance) ->
        $.mobile.loading("show", text: "Generating ER Diagram...", textVisible: true, textonly: false)
        gui.gui_instance.hide_toolbar()
        gui.gui_instance.switch_to_erd()
        json = JSON.stringify(@diag.to_json())
        @serverconn.request_meta2erd_translation(json,(data)->
            gui.gui_instance.current_gui.diagadapter.import_jsonstr(data)
        )
        $.mobile.loading("hide")



exports.gui.GUIORM = GUIORM
