# widgetmgr.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.gui = exports.gui ? {}


# @namespace gui
class WidgetMgr
    constructor: () ->

# @namespace gui
#
# Common widgets used in all GUIs (UML, EER, etc.).
class CommonWidgets extends WidgetMgr
    constructor: () ->
        super()
        # Widgets that are the same for all type of GUIImpl.
        # Login
        @loginwidget = new login.LoginWidgetView(
            {el: $("#loginwidget_placer")})
        # Save-Load
        # Error reporting widget
        @errorwidget = new views.common.ErrorWidgetView(
            {el: $("#errorwidget_placer")})
        # Details page elements
        @owllinkinsert = new views.common.OWLlinkInsertView(
            {el: $("#owllink_placer")})
        @exportjsonwidget = new views.common.ExportJSONView(
            {el: $("#exportjson_placer")})
        @obdawidget = new views.common.OBDAView(
            {el: $("#OBDA_widget_placeholder")})
        @importjsonwidget = new views.common.ImportJSONView(
            {el: $("#importjsonwidget_placer")})
        @trafficlight = new views.common.TrafficLightsView(
            {el: $("#trafficlight")})
        @donewidget = new views.common.DoneWidget(
            {el: $("#donewidget_placeholder")})
        @translationwidget = new views.common.TranslationWidget(
            {el: $("#translation_placeholder")})
        @reasoningdetailswidget = new views.common.ReasoningDetailsWidget(
            {el: $("#reasoning_placeholder")})
        @clearwidget = new views.common.ClearWidget(
            {el: $("#clearwidget_placeholder")})
        @metamodelwidget = new views.common.MetamodelView(
            {el: $("#metamodel_placeholder")})
        @namespacewidget = new views.common.NamespacesView(
            el: $("#namespaces_placeholder"))

    # Report an error to the user.
    #
    # @param status {String} the status text.
    # @param error {String} error message
    show_error: (status, error) ->
        @errorwidget.show status, error

    # Show a "done" widget on a Joint cell.
    #
    # @param cellid {string} The Joint cellid.
    # @param callback {function} A callback function without parameters.
    show_donewidget: (cellid, callback=null) ->
        viewpos = graph.getCell(cellid).findView(paper).getBBox()
        top = viewpos.y + viewpos.height * 2
        left = viewpos.x + viewpos.width/2

        @donewidget.show(
            x: left
            y: top,
            callback
        )


    #
    # Put the traffic light on green.
    traffic_light_green: () ->
        @trafficlight.turn_green()

    #
    # Put the traffic light on red.
    traffic_light_red: () ->
        @trafficlight.turn_red()

    #
    # Put the traffic light on red.
    traffic_light_yellow: () ->
        @trafficlight.turn_yellow()

    #
    # Show the "Import JSON" modal dialog.
    #UML
    show_import_json: () ->
        gui.gui_instance.hide_toolbar()
        @importjsonwidget.show()


    # Set the OWLlink dara at the "Insert OWLlink" section.
    #
    # @param str {string} The OWLlink data.
    set_insert_owllink: (str) ->
        @owllinkinsert.set_owllink(str)

    # Update and show the "Export JSON String" section.
    show_export_json: () ->
        @exportjsonwidget.set_jsonstr(
            gui.gui_instance.current_gui.diagadapter.diag_to_json())
        $(".exportjson_details").collapsible("expand")
        gui.gui_instance.change_to_details_page()

    # Refresh the content of the "Export JSON String" section.
    #
    # No need to show it.
    refresh_export_json: () ->
      @exportjsonwidget.set_jsonstr(
        gui.gui_instance.current_gui.diagadapter.diag_to_json())

    refresh_ns: () ->
      @namespacewidget.refresh_namespace(model.inamespace)

    reset_ns: () ->
      @namespacewidget.clear_namespace()

# @namepsace gui
#
# Widgets for the ORM GUI only.
class ORMWidgets extends WidgetMgr
    construct: () ->
        super()
        @crearclase = new CreateClassView({el: $("#crearclase")});
        @editclass = new EditClassView({el: $("#editclass")})
        @classoptions = new ClassOptionsView({el: $("#classoptions")})
        @relationoptions = new RelationOptionsView({el: $("#relationoptions")})
        @isaoptions = new IsaOptionsView({el: $("#isaoptions")})

    # Set the class Id of the class options GUI.
    set_options_classid: (model_id) ->
        @relationoptions.set_classid(model_id)
        @classoptions.set_classid(model_id)
        @isaoptions.set_classid(model_id)

    # Hide the class options GUI.
    hide_options: () ->
        @classoptions.hide()
        @relationoptions.hide()
        @editclass.hide()
        @isaoptions.hide()



exports.gui.WidgetMgr = WidgetMgr
exports.gui.CommonWidgets = CommonWidgets
exports.gui.ORMWidgets = ORMWidgets
