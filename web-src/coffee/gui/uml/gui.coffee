# gui.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.gui = exports.gui ? {}
exports.gui.uml = exports.gui.uml ? {}

# @namespace gui.uml
#
# Central GUI *do-it-all* class...
#
class GUIUML extends gui.GUIIMPL

    # Create a GUIUML instance.
    #
    # @param {JointJS.Graph } graph The JointJS Graph used for drawing models.
    # @param {JointJS.Paper} paper The JointJS Paper used for drawing views.
    constructor: (graph = null, paper = null) ->
        super(graph, paper)
        @guiname = "UML"
        # @property [String] The URL prefix.
        @urlprefix = ""
        # @property [UMLDiagram] The user model diagram representation.

        @diag = new model.uml.UMLDiagram(@graph)

        @state = gui.get_state().selection_state()

        @diagadapter = null
        @widgets = null

        @serverconn = new ServerConnection( (jqXHR, status, text) ->
            exports.gui.gui_instance.widgets.show_error(
                status + ": " + text , jqXHR.responseText)
        )

    disable: () ->
        super()
        @widgets.disable()

    enable: () ->
        super()
        @widgets.enable()

    # set_graph: (graph) ->
    #     super(graph)
    #     @diag.set_graph(graph)

    # set_paper: (paper) ->
    #     super(paper)
    #     @diagadapter.set_paper(paper)

    set_urlprefix: (str) -> @urlprefix = str


    # What to do when the user clicked on a cellView.
    on_cell_clicked: (cellview, event, x, y) ->
        @state.on_cell_clicked(cellview, event, x, y, this)

    # Update the interface with satisfiable information.
    #
    # @param data {string} is a JSON string with the server response.
    update_satisfiable: (data) ->
        console.log "Data Recieved:"
        console.log data
        obj = JSON.parse(data);
        this.set_trafficlight(obj)
        $("#reasoner_input").html(obj.reasoner.input)
        $("#reasoner_output").html(obj.reasoner.output)
        @diagadapter.set_unsatisfiable(obj.unsatisfiable.classes)
        @diagadapter.set_satisfiable(obj.satisfiable.classes)

    # Set the traffic-light according to the JSON object recived by the server.
    #
    # @param obj {JSON} The JSON object parsed from the recieved data.
    set_trafficlight: (obj) ->
        if (obj.satisfiable.kb)
            if (obj.unsatisfiable.classes.length == 0)
                gui.gui_instance.widgets.traffic_light_green()
            else
                gui.gui_instance.widgets.traffic_light_yellow()
        else
            gui.gui_instance.widgets.traffic_light_red()

    # Send a POST to the server for checking if the diagram is
    # satisfiable.
    check_satisfiable: () ->
        @serverconn.request_satisfiable @diagadapter.diag_to_json(),
            (data) ->
                gui.uml.iguiuml.update_satisfiable(data) # Be careful with the context
            # change! this will have another object...
        gui.uml.iumlwidgets.hide_reasoning()

    # Update the translation information on the GUI and show it to the
    # user.
    #
    # Depending on the format selected by the user show it as HTML or
    # inside a textarea tag.
    #
    # @param data {string} The HTML, OWLlink or the translation
    # string.
    # @see CreateClassView#get_translation_format
    update_translation: (data) ->
        console.log 'Data recieved:'
        console.log data
        gui.gui_instance.current_gui.widgets.translatewidget.set_text data

    # Update traffic light and models after full reasoning
    #
    # Draft version: only update textarea with data
    #
    # @param data {string} OWLlink string
    update_full_reasoning: (data) ->
        console.log "Data recieved:"
        console.log(data)
        obj = JSON.parse data
        if obj.satisfiable.kb
            gui.gui_instance.widgets.traffic_light_green()
        else
            gui.gui_instance.widgets.traffic_light_red()
#        $("#owllink_source").text(data)
        @diagadapter.set_unsatisfiable obj.unsatisfiable.classes
        @diagadapter.set_satisfiable obj.satisfiable.classes
        gui.gui_instance.widgets.reasoningdetailswidget.set_kb obj.satisfiable.kb
        gui.gui_instance.widgets.reasoningdetailswidget.set_reasoner_output obj.reasoner.output
        gui.gui_instance.widgets.reasoningdetailswidget.set_sat obj.satisfiable.classes
        gui.gui_instance.widgets.reasoningdetailswidget.set_unsat obj.unsatisfiable.classes

        #import json luego de razonar con posiblemente sugerencias!

    update_obda: (data) ->
      parsed = $.parseJSON(data)
      console.log("Data recieved:")

      if(parsed.query_answer === ""){
        $("#sparql_output").val("void");
      }else{
        $("#sparql_output").val(parsed.query_answer);
      }
    


    update_full_obda: (data) ->
      parsed = $.parseJSON(data)
      console.log("Data recieved:")

      if(parsed.mappings === ""){
        $("#mappings").val("void");
      }else{
        $("#mappings").val(parsed.mappings);
      }
      if(parsed.query_answer === ""){
        $("#sparql_output").val("void");
      }else{
        $("#sparql_output").val(parsed.query_answer);
      }
    

    update_metamodel: (data) ->
        console.log(data)
        $("#owllink_source").text(data)
        $("#owllink_source").show()
        $("#html-output").hide()
        gui.gui_instance.change_to_details_page()

    update_fol: (data) ->
        console.log(data)
        $("#source_verbalisation").text(data)
        $("source_verbalisation").show()
        $("#html-output").hide()
        this.change_to_verbalisation_page()

    # Translate the current model into a formalization.
    #
    # Show the user a "wait" message while the server process the model.
    #
    # @param strategy {String} The strategy name to use for formalize the model.
    # @param syntax {String} The output sintax format.
    translate_formal: (strategy, syntax) ->
        console.log 'Asking server for formalization using ' +
            strategy + ' in syntax ' + syntax
  #      json = JSON.stringify @diag.to_json()
        json = @diag.to_json()
        console.log $("#insert_owllink_input").val()
        json.owllink = $("#insert_owllink_input").val()
        jsonstr = JSON.stringify json
        @serverconn.request_translation jsonstr, syntax, strategy, (data) ->
            gui.gui_instance.update_translation(data)

    # Full reasoning on the current model and selecting a reasoner.
    #
    # Show the user a "wait" message while the server process the model.
    #
    # @param strategy {String} model encoding required for reasoning on.
    # @param syntax {String} reasoning system.
    full_reasoning: (strategy, reasoner) ->
        json = @diag.to_json()
        json.owllink = $("#insert_owllink_input").val()
        jsonstr = JSON.stringify json
        @serverconn.request_full_reasoning jsonstr, strategy, reasoner, (data) ->
            gui.uml.iguiuml.update_full_reasoning(data)

    

    OBDA: () ->
        obdaFramework = $("#obdaFramework").val()
        mappings = $("#mappings").val()
        json = JSON.stringify @diag.to_json()
        query = $("#sparql_input").val()
        dbIP = $("#dbIp").val()
        dbPassword = $("#dbPassword").val()
        dbDriver = ("#dbDriver").val()
        dbName = ("#dbName").val()
        dbSchema = ("dbSchema").val
        @serverconn.request_obda json, mappings, obdaFramework, query, dbIp, dbPassword, dbDriver, dbName, dbSchema, (data) ->
            gui.uml.iguiuml.update_obda(data)


    full_OBDA: () ->
        mappingsTool = $("#mappingsTool").val();
        obdaFramework = $("#obdaFramework").val()        
        json = JSON.stringify @diag.to_json()
        query = $("#sparql_input").val()
        dbIP = $("#dbIp").val()
        dbPassword = $("#dbPassword").val()
        dbDriver = ("#dbDriver").val()
        dbName = ("#dbName").val()
        dbSchema = ("dbSchema").val
        @serverconn.request_full_obda json, mappingsTool, obdaFramework, query, dbIp, dbPassword, dbDriver, dbName, dbSchema, (data) ->
            gui.uml.iguiuml.update_full_obda(data)



    import_owl: (owl) ->
        @serverconn.request_owl2importer(owl, (data)->
          gui.gui_instance.current_gui.diagadapter.import_jsonstr(data)
        )


    # Event handler for translate diagram to OWLlink using Ajax
    # and the api/translate/berardi.php translator URL.
    #
    # @deprecated Use translate_formal() instead.
    translate_owllink: (gui_instance) ->
        format = @widgets.crearclase.get_translation_format()
        strat = @widgets.crearclase.get_translation_strategy()
        this.translate_formal(strat, format)


    change_to_details_page: () ->
        $.mobile.changePage("#details-page", transition: "slide")

    change_to_verbalisation_page: () ->
        $.mobile.changePage("#verbalisation-page", transition: "slide")

    change_to_diagram_page: () ->
      @widgets.hide_options()
#        $.mobile.changePage("#diagram-page", transition: "slide", reverse: true)
    #
    # Hide the left side "Tools" toolbar
    #
    hide_toolbar: () ->
        $("#tools-panel [data-rel=close]").click()

    hide_umldiagram_page: () -> $("#diagram-page").css("display","none")

    show_umldiagram_page: () -> $("#diagram-page").css("display","block")

    # Change the interface into a "new association" state.
    #
    # @param class_id {string} The id of the class that triggered it and thus,
    #   the starting class of the association.
    # @param mult {array} An array of two strings representing the cardinality from and to.
    set_association_state: (class_id, mult, roles, name, with_class, type) ->
        @widgets.hide_options()
        @state = gui.state_inst.association_state()
        @state.set_cellStarter class_id
        @state.set_cardinality mult
        @state.set_roles roles
        @state.set_name name
        @state.set_with_class with_class
        @state.set_type type

        if with_class
          @state.enable_with_class()

        if type != "binary"
          gui.gui_instance.widgets.show_donewidget class_id, () ->
            gui.gui_instance.current_gui.diagadapter.add_relationship_aux(class_id, name, mult, roles, with_class)

    # Change to the IsA GUI State so the user can select the child for the parent.
    #
    # @param class_id {String} The JointJS.Cell id for the parent class.
    # @param disjoint {Boolean} optional. If the relation has the disjoint constraint.
    # @param covering {Boolean} optional. If the relation has the disjoint constraint.
    set_isa_state: (class_id, disjoint = false, covering = false) ->
        @widgets.hide_options()
        @state = gui.state_inst.isa_state()
        @state.set_cellStarter class_id
        @state.set_constraint disjoint, covering

        gui.gui_instance.widgets.show_donewidget(class_id, () ->
            gui.gui_instance.current_gui.diagadapter.add_subsumption_childs(class_id))


    # Change the interface into a "selection" state.
    set_selection_state: () ->
        @state = gui.state_inst.selection_state()

    to_metamodel: () ->
        $.mobile.loading("show", text: "Metamodelling...", textVisible: true, textonly: false)
        json = JSON.stringify(@diag.to_json())
        @serverconn.request_metamodel_translation(json,this.update_metamodel)

    to_fol: () ->
      $.mobile.loading("show", text: "FOL...", textVisible: true, textonly: false)
      json = JSON.stringify(@diag.to_json())
      @serverconn.request_fol_translation(json,this.update_fol)



# Unique instance of GUIUML
#
# @namespace gui.uml
exports.gui.uml.iguiuml = null

# Create needed objects for initializing the gui.uml package.
#
# @param {joint.dia.Graph} graph
# @param {joint.dia.Paper} paper
# @namespace gui.uml
exports.gui.uml.initialize = (graph, paper) ->
    gui.uml.iguiuml = new gui.uml.GUIUML graph, paper
    gui.uml.iumladapter = new gui.uml.UMLAdapter gui.uml.iguiuml.diag, paper

    # TODO: Remove this. Is deprecated.
    gui.uml.iguiuml.diagadapter = gui.uml.iumladapter
    gui.uml.iguiuml.widgets = gui.uml.iumlwidgets


exports.gui.uml.GUIUML = GUIUML

```
joint.shapes.uml.AssociationIcon = joint.shapes.basic.Generic.extend({
    markup: '<g class="rotatable"><g class="scalable"><polygon class="body"/></g><text/></g>',
    defaults: joint.util.deepSupplement({
        type: 'uml.AssociationIcon',
            attrs: {
                '.body': {
                    fill: 'grey', stroke: 'black', 'stroke-width': 2,
                    points: '40,0 80,40 40,80 0,40'
                },
                text:{'ref': '.body', 'ref-y': .5, 'ref-x': .5,
                'font-weight': 'bold',
                'text-anchor': 'middle',
                'y-alignment': 'middle',
                'fill': 'black', 'font-size': 14, 'font-family': 'Times New Roman'}
              }
        }, joint.shapes.basic.Generic.prototype.defaults)
    });

joint.shapes.uml.GeneralizationIcon = joint.shapes.basic.Generic.extend({
  markup: '<g class="rotatable"><g class="scalable"><g class="labels"/><circle class="body"/></g></g>',
  defaults: joint.util.deepSupplement({
    type: 'uml.GeneralizationIcon',
    size: { width: 25, height: 25},
    attrs: {
      '.body': {
        r: 50,
        cx: 50,
        stroke: 'black',
        fill: 'grey',
        'stroke-width': 2
      }}}, joint.shapes.basic.Generic.prototype.defaults)
});

joint.shapes.uml.LinkWithClass = joint.dia.Link.extend({
    markup : ['<path class="connection" stroke="black" d="M 0 0 0 0"/>','<path class="connection-wrap" d="M 0 0 0 0"/>','<g class="labels"/>','<g class="marker-vertices"/>','<g class="marker-arrowheads"/>'].join(''),
    defaults: {  type: 'uml.LinkWithClass', attrs:{'.connection': { stroke: 'black', 'stroke-width': 2, 'stroke-dasharray': '5 2'}
}}});

joint.shapes.uml.GeneralizationChildLink = joint.dia.Link.define("uml.GeneralizationChildLink");

joint.shapes.uml.Link = joint.dia.Link.extend({ markup : ['<path class="connection" stroke="black" d="M 0 0 0 0"/>','<path class="connection-wrap" d="M 0 0 0 0"/>','<g class="labels"/>','<g class="marker-vertices"/>','<g class="marker-arrowheads"/>'].join(''), defaults: { type: 'ovm.Link', attrs:{'.connection': { stroke: 'black', 'stroke-width': 2}}}});
```
