# widgets.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.gui = exports.gui ? {}
exports.gui.uml = exports.gui.uml ? {}

# @namespace gui.uml
#
# Widgets used in UML GUI only.
class UMLWidgets extends gui.WidgetMgr
    constructor: () ->
        super()
        @crearclase = new views.uml.classes.CreateClassView
            el: $("#crearclase_placeholder")
        @toolbar = new views.uml.ToolsUML
            el: $("#lang_tools_placeholder")
        @editclass = new views.uml.classes.EditClassView
            el: $("#editclass_placeholder")
        @editattrs = new views.uml.classes.EditAttributes
            el: $("#uml_editattr_placer")
        @classoptions = new views.uml.classes.ClassOptionsView
            el: $("#classoptions_placeholder")
        @relationoptions = new views.uml.association.AssociationOptionsView
            el: $("#relationoptions_placeholder")
        @isaoptions = new views.uml.generalisation.GeneralisationOptionsView
            el: $("#isaoptions_placeholder")
        @translatewidget = new views.uml.TranslateWidget
            el: $("#uml_translate_placeholder")
        @reasoningwidget = new views.uml.ReasoningWidget
            el: $("#uml_reasoning_placeholder")
        @importowlwidget = new views.uml.ImportOWLView(
            {el: $("#uml_importowlwidget_placer")})
        @naryoptionswidget = new views.uml.association.NaryOptionsView(
            el: $("#uml_naryoptions_placeholder"))
        @binaryoptionswidget = new views.uml.association.BinaryOptionsView(
            el: $("#uml_binaryoptions_placeholder"))
        @assoctypewidget = new views.uml.association.AssociationTypeView(
            el: $("#uml_assoctype_placeholder"))
        @assocIconOptions = new views.uml.association.AssocIconOptionsView
            el: $("#associconoptions")
        @genIconOptions = new views.uml.generalisation.GenIconOptionsView
            el: $("#generalisationiconoptions")
        @editAssoc = new views.uml.association.EditAssocView
            el: $("#editassoc")
        @infowidget = new views.uml.InfoWidgetView(
            {el: $("#infowidget_placer")})

    disable: () ->
        # @crearclase.disable()
        @toolbar.disable()
        # @editclass.disable()
        # @editattrs.disable()
        # @classoptions.disable()
        # @relationoptions.disable()
        # @isaoptions.disable()

    enable: () ->
        # @crearclase.enable()
        @toolbar.enable()
        # @editclass.enable()
        # @editattrs.enable()
        # @classoptions.enable()
        # @relationoptions.enable()
        # @isaoptions.enable()

    # Set the class Id of the class options GUI.
    #
    # Used as handler for the SelectionState.on_click
    set_options_classid: (model_id) ->
        # @relationoptions.set_classid(model_id)
        if gui.uml.iumladapter.diag.find_class_by_classid(model_id) != undefined
          @classoptions.set_classid(model_id)
        else
            if gui.uml.iumladapter.diag.find_generalization_by_id(model_id) != undefined
              @genIconOptions.set_geniconid(model_id)
            else
              if gui.uml.iumladapter.diag.find_association_by_id(model_id) != undefined
                @assocIconOptions.set_reliconid(model_id)

        # @isaoptions.set_classid(model_id)

    # Hide the class options GUI.
    hide_options: () ->
        @classoptions.hide()
        @relationoptions.hide()
        @editclass.hide()
        @isaoptions.hide()
        @genIconOptions.hide()
        @assocIconOptions.hide()

    set_editclass_classid: (model_id) ->
        # editclass = new EditClassView({el: $("#editclass")})
        @editclass.set_classid(model_id)
        @editclass.show()

    # Tell the editattr widget which class it should edit.
    #
    # @param model_id [string] The model (class primitive) to edit.
    set_editattribute_classid: (model_id) ->
        @editattrs.set_classid(model_id)
        @editattrs.show()

    # Tell the @relationoptions widget which class it should edit.
    #
    # @param model_id [string] The model (class primitive) to edit.
    set_editgeneralization_classid: (model_id) ->
        @isaoptions.set_classid(model_id)
        @isaoptions.show()

    # Show wigets for creating a new association.
    #
    # @param model_id [string] The model (class primitive) to edit.
    # @param type [string] Can be "binary", "n-ary" or null.
    set_editassociation_classid: (model_id, type = null) ->
        if type == null
            @assoctypewidget.set_classid model_id
            @assoctypewidget.show()
        if type == 'binary'
            @binaryoptionswidget.set_classid model_id
            @binaryoptionswidget.show()
        if type == 'n-ary'
            @naryoptionswidget.set_classid model_id
            @naryoptionswidget.show()
        if type == "edit"
            @editAssoc.set_reliconid model_id
        if type == "add"
            @addClass.set_reliconid model_id


    show_reasoning: () ->
      @reasoningwidget.show()

    hide_reasoning: () ->
      @reasoningwidget.hide()

    show_translation: () ->
      @translatewidget.show()

    hide_translation: () ->
      @translatewidget.hide()

    show_info: (info) ->
      @infowidget.show info

    show_importowl: () ->
      @importowlwidget.show()

    hide_importowl: () ->
      @importowlwidget.hide()

# Unique instance of UMLWidgets
#
# @namespace gui.uml
exports.gui.uml.iumlwidgets = new UMLWidgets()

exports.gui.uml.UMLWidgets = UMLWidgets
