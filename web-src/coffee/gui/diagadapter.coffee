# diagadapter.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.gui = exports.gui ? {}

# @namespace gui
#
# Adapter for providing proper parameters to the diagram Model and
# its interaction with the interface.
class DiagAdapter
    constructor: (@diag, @paper) ->

    set_paper: (@paper) ->

    # Add an attribute
    add_attribute: (hash_data) ->

    add_object_type: (hash_data) ->

    add_relationship: (class_a_id, class_b_id, name = null, mult = null, roles = null) ->

    add_relationship_aux: (class_a_id, name = null, mult = null, roles = null, with_class = null) ->

    add_relationship_attr: (class_id, attribute_id, name = null)->

    add_subsumption: (class_id, isa_id, name) ->

    # Change a class name identified by its classid.
    #
    # @example Getting a classid
    #   < graph.getCells()[0].id
    #   > "5777cd89-45b6-407e-9994-5d681c0717c1"
    #
    # @param class_id {string}
    # @param name {string}
    edit_class_name: (class_id, name) ->
        # Set the model name
        # cell = @graph.getCell(class_id)
        # cell.set("name", name)
        @diag.rename_class(class_id, name)

        # Update the view
        @diag.update_view(class_id, @paper)

    #
    # Delete a class from the diagram.
    #
    # @param class_id {string} a String with the class Id.
    delete_class: (class_id) ->
      @diag.delete_class_by_classid(class_id)


    show_class_info: (class_id) ->

    show_gen_info: (gen_id) ->

    show_assoc_info: (assoc_id) ->

    # Import a JSON string.
    #
    # This will not reset the current diagram, just add more elements.
    #
    # # GUIIMPL Subclasses
    # This messages does not need to be reimplemented in GUIIMPL subclasses
    #
    # @param jsonstr {String} a JSON string, like the one returned by
    #     #diag_to_json().
    # @see #import_json
    import_jsonstr: (jsonstr) ->
        console.log jsonstr
        json = JSON.parse(jsonstr)
        # Importing the Diagram
        @import_json(json)

    # Import a JSON object.
    #
    # This will not reset the current diagram, just add more elements.
    #
    # Same as import_jsonstr, but it accept a JSON object as parameter.
    #
    # @param json_obj {JSON object} A JSON object.
    import_json: (json_obj) ->
        @diag.import_json(json_obj)
        # Importing owllink
        gui.gui_instance.widgets.owllinkinsert.append_owllink(
            "\n" + json_obj.owllink)

    # Reset all the diagram and the input forms.
    #
    # Reset the diagram and the "OWLlink Insert" input field.
    reset_all: () ->
        gui.gui_instance.widgets.owllinkinsert.set_owllink("")
        @diag.reset()
        gui.gui_instance.current_gui.hide_toolbar()

    # Generate a JSON string representation of the current diagram.
    #
    # @return {string} A JSON string.
    diag_to_json: () ->
        json = @diag.to_json()
        return JSON.stringify(json)

    # Show these classes as unsatisifable.
    #
    # @param classes_list {Array<String>} a list of classes names.
    set_unsatisfiable: (classes_list) ->
        @diag.set_unsatisfiable(classes_list)

    # Show these classes as satisifable.
    #
    # @param classes_list {Array<String>} a list of classes names.
    set_satisfiable: (classes_list) ->
        @diag.set_satisfiable(classes_list)




# @namespace gui
#
# DiagAdapter for ORM diagrams.
class ORMAdapter extends DiagAdapter
    constructor: (diag, paper) ->
        super(diag, paper)

    #
    # Add a class to the diagram.
    #
    # @param hash_data {Hash} data information for creating the Class. Use `name`, `attribs` and `methods` keys.
    # @see Class
    # @see Diagram#add_class
    add_object_type: (hash_data) ->
        gui.gui_instance.hide_toolbar()
        @diag.add_class(hash_data)

    #
    # Add a simple association from A to B.
    # Then, set the selection state for restoring the interface.
    #
    # @example Getting a classid
    #   < graph.getCells()[0].id
    #   > "5777cd89-45b6-407e-9994-5d681c0717c1"
    #
    # @param class_a_id {string}
    # @param class_b_id {string}
    # @param name {string} optional. The association name.
    # @param mult {array} optional. An array of two string with the cardinality from class and to class b.
    add_relationship: (class_a_id, class_b_id, name=null, mult=null) ->
        @diag.add_association(class_a_id, class_b_id, name, mult)
        gui.gui_instance.current_gui.set_selection_state()

    # Add a Generalization link and then set the selection state.
    #
    # @param class_parent_id {string} The parent class Id.
    # @param class_child_id {string} The child class Id.
    #
    # @todo Support various children on parameter class_child_id.
    add_subsumption: (class_parent_id, class_child_id, disjoint=false, covering=false) ->
        @diag.add_generalization(class_parent_id, class_child_id, disjoint, covering)
        gui.gui_instance.current_gui.set_selection_state()




exports.gui.DiagAdapter = DiagAdapter

exports.gui.ORMAdapter = ORMAdapter
