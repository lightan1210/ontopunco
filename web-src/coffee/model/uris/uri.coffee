# class.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uris = exports.model.uris ? {}

# A Class from our model UML diagram.
#
# @namespace model.uml
class URI extends model.Namespaces

    constructor : () ->
      super(model.inamespace.get_c_namespaces())
      @name = null
      @prefix = null
      @url = null
      @nspace_obj = null
      this.init_uri()

    # URIs are initialised with default ontology prefix and url
    #
    init_uri: () ->
      @url = @get_ontologyIRI_value()
      @prefix = @get_ontologyIRI_prefix()
      @set_nspace_obj()

    refresh_uri: () ->
      @url = @nspace_obj.value
      @prefix = @nspace_obj.prefix

    set_nspace_obj: () ->
      @nspace_obj = @link_URI_to_namespace(@prefix, @url)

    set_URI: (strn) ->
      nname = null
      nname = this.has_fullname(strn)

      if not nname?   # it's not a fullname. nname is null
        urip = null
        urip = this.get_prefix(strn)  # return the corresponding URI if name is prefixed

        if not urip?   # it's not a fullname or a prefixed name. urip is null
          url = this.get_ontologyIRI_value()
          pr = this.get_ontologyIRI_prefix()
          this.set_name(strn)
          this.set_prefix(pr)
          this.set_url(url)
          @set_nspace_obj()
        else           # it is a prefix
          nname = this.get_name_from_prefix(strn)
          this.set_name(nname)
          ns = null
          ns = this.find_default_ns_by_prefix(urip)

          if ns?   # if prefix is defined in namespaces and it is a default one
            url = this.get_url_str(ns)
            this.set_prefix(urip)
            this.set_url(url)
            @set_nspace_obj()
          else
            ns = this.find_custom_ns_by_prefix(urip)
            if ns? # if prefix is defined in namespaces and it is a custom one
              url = this.get_url_str(ns)
              this.set_prefix(urip)
              this.set_url(url)
              @set_nspace_obj()
            else
              if this.is_ontologyIRI_prefix(urip)
                url = this.get_ontologyIRI_value()
                this.set_prefix(urip)
                this.set_url(url)
                @set_nspace_obj()
              else
                this.set_prefix(urip)
                this.set_url("")
                @set_nspace_obj()
      else    # it is a full name
        urlstr = null
        this.set_name(nname)
        urlstr = this.get_URL(strn)

        if urlstr?
          ns = null
          ns = this.find_default_ns_by_url(urlstr)

          if ns?   # if url is defined in namespaces and it is a default one
            p = this.get_prefix_str(ns)
            this.set_prefix(p)
            this.set_url(urlstr)
            @set_nspace_obj()
          else
            ns = this.find_custom_ns_by_url(urlstr)
            if ns? # if url is defined in namespaces and it is a custom one
              p = this.get_prefix_str(ns)
              this.set_prefix(p)
              this.set_url(urlstr)
              @set_nspace_obj()
            else
              if this.is_ontologyIRI_value(urlstr)
                p = this.get_ontologyIRI_prefix()
                this.set_prefix(p)
                this.set_url(urlstr)
                @set_nspace_obj()
              else
                this.set_prefix("")
                this.set_url(urlstr)
                @set_nspace_obj()


    set_name: (name) ->
      @name = name

    set_prefix: (prefix) ->
      @prefix = prefix

    set_url: (url) ->
      @url = url

    get_name_uri: () ->
      return @name

    get_prefix_uri: () ->
      return @prefix

    get_url_uri: () ->
      return @url

    get_fullname: () ->
      return @url.concat @name

    get_prefixedname: () ->
      return @prefix.concat @name


exports.model.uris.URI = URI
