# link.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A Link between two classes.
#
# @namespace model.uml
class BinaryAssociation extends model.Link
    # @param classes {Array<Class>} An array of Class objects,
    #   the first class is the "from" and the second is the "to" class
    #   in a two-linked relation.
    constructor: (classes, name = null) ->
        if name?
            super(classes, name)
        else
            super(classes, BinaryAssociation.get_new_name())

         @role_from = null
         @role_to = null


    # Set the roles. Roles are first citizen objects so that more resposabilities are assigned.
    #
    # @param [string] user_d_role is a user define role for the current link.
    # @param [string] class_r is the class associated to the current role. This name is also used as default role name.
    # @param [string] mult is the cardinality for the current role.
    set_role_from: (user_d_role, class_r, mult) ->
      @role_from = new model.uml.Role()

      if user_d_role == undefined or user_d_role == ""
         @role_from.update_URI(class_r.get_name().toLowerCase())
      else
        @role_from.update_URI(user_d_role.toLowerCase())

      @role_from.set_mult(mult)
      @role_from.set_class(class_r)


    # Set the roles. Roles are first citizen objects so that more resposabilities are assigned.
    #
    # @param [string] user_d_role is a user define role for the current link.
    # @param [string] class_r is the class associated to the current role. This name is also used as default role name.
    # @param [string] mult is the cardinality for the current role.
    set_role_to: (user_d_role, class_r, mult) ->
      @role_to = new model.uml.Role()

      if user_d_role == undefined or user_d_role == ""
         @role_to.update_URI(class_r.get_name().toLowerCase())
      else
        @role_to.update_URI(user_d_role.toLowerCase())

      @role_to.set_mult(mult)
      @role_to.set_class(class_r)

    get_role: () ->
      return [@role_from, @role_to]

    # Following functions returns each one of the roles associated
    # "u" roles mean user defined roles and f = from/t = to.
    # "d" roles mean default roles and f = from/t = to. They take the class name.
    get_role_from: () ->
      return @role_from

    get_role_to: () ->
      return @role_to

    change_mult_to_null : (mult, index) ->
        if (mult == "0..*") or (mult == "0..n")  or  (mult == "") or  (mult == undefined)
            @mult[index] = null

    change_roles_to_null : (roles, index) ->
          if (roles == "0..*") or (roles == "0..n")  or  (roles == "") or  (roles == undefined)
              @roles[index].update_URI(null)
    #
    # @param class_from an instance of Class.
    set_from : (class_from) ->
        @classes[0] = class_from

    get_from : () ->
        return @classes[0]

    set_to : (class_to) ->
        @classes[1] = class_to

    get_to: () ->
        return @classes[1]

    get_classes: () ->
        return @classes

    # True if a two-linked relation. False otherwise.
#    is_two_linked: () ->
#        return @classes.length == 2

    # *Implement in the subclass if necesary.*
    #
    # @param parentclass {Class} The Class instance to check.
    # @return `true` if this is a generalization class and has the given parent instance. `false` otherwise.
    has_parent: (parentclass) ->
        return false

    # Check is binary association is with class. However, this function returns always false for links.
    #
    # @return false
    has_association_class:() ->
      return false

    # Is this link associated to the given class?
    #
    # @param c {Class instance} The class to test with.
    #
    # @return {Boolean}
    is_associated: (c) ->
        this.has_parent(c) or @classes.includes(c)

    to_json: () ->
        json = super()
        json.classes = $.map(@classes, (myclass) ->
            myclass.get_fullname()
        )
        json.multiplicity = [@role_from.get_mult(), @role_to.get_mult()]
        roles = [@role_from.get_fullname(), @role_to.get_fullname()]

        json.roles = roles
        json.type = "association"

        return json

    #
    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if @joint == null
            @joint = []
            if csstheme != null
                @joint.push(factory.create_binary_association(
                    @classes[0].get_classid(),
                    @classes[1].get_classid(),
                    @name,
                    csstheme.css_links,
                    @role_from.get_mult(),
                    @role_to.get_mult()))
            else
                @joint.push(factory.create_binary_association(
                    @classes[0].get_classid(),
                    @classes[1].get_classid(),
                    @name
                    null,
                    @role_from.get_mult(),
                    @role_to.get_mult()))



    # Compare the relevant elements of these links.
    #
    # @param other {Link}
    # @return {boolean}
    same_elts: (other) ->
        if !super(other)
            return false
        if @classes.length != other.classes.length
            return false

        # it must have the same order!
        all_same = true
        for index in [0...@classes.length]
            do (index) =>
                all_same = all_same && @classes[index].same_elts(other.classes[index])

        all_same = all_same && v == other.mult[k] for v,k in @mult
        all_same = all_same && v == other.roles[k] for v,k in @roles

        return all_same


BinaryAssociation.get_new_name = () ->
    if BinaryAssociation.name_number == undefined
        BinaryAssociation.name_number = 0
    BinaryAssociation.name_number = BinaryAssociation.name_number + 1
    return "r" + BinaryAssociation.name_number


exports.model.uml.BinaryAssociation = BinaryAssociation
