# generalization.coffee --
# Copyright (C) 2017 Giménez, Christian, Angela Oyarzun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A link to generalizationIcon.
#
# @namespace model.uml
class LinkToGeneralization extends model.Link

    # @param parent_class {Class} The parent class.
    # @param classes {Class} Child class.
    constructor: (classes, name = null) ->
        super(classes, name)
        @disjoint=false
        @covering=false

    get_class: () ->
      return @classes[0]

    get_gen: () ->
      return @classes[1]

    remove_class_from_gen: (link) ->
      c = this.get_class()

      @classes = @classes.filter( (elt, index, arr) ->
            elt != c
      )

    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if csstheme == null
            csstheme =
                css_links: null
        if @joint == null
            @joint = []
            @joint.push(factory.create_to_generalization_link(
                @classes[0].get_classid(),
                @classes[1].get_relid(),
                @name,
                @disjoint,
                @covering))

    set_disjoint: (@disjoint) ->
    set_covering: (@covering) ->

    to_json: () ->

exports.model.uml.LinkToGeneralization = LinkToGeneralization
