# generalization.coffee --
# Copyright (C) 2017 Giménez, Christian, Angela Oyarzun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A generalizationIcon.
#
# @namespace model.uml
class Generalization extends model.MyModel

    # @param parent_class {Class} The parent class.
    # @param classes {Array<Class>} An array of child classes.
    constructor: (@parent_class, @classes, name = null, @position) ->
        if name?
            super(name)
        else
            super(Generalization.get_new_name())

        @disjoint = false
        @covering = false
        @targetCells = []


    get_gen_labels: () ->
      name = 'URI Gen: '.concat @get_uri().get_fullname()
      prefix = 'Prefix: '.concat @get_uri().get_prefix_uri()
      url = 'URL: '.concat @get_uri().get_url_uri()
      covering = 'Covering: '.concat @get_covering()
      disjoint = 'Disjoint: '.concat @get_disjoint()
      return [[name, prefix, url, covering, disjoint],[]]

    get_joint: (factory = null, csstheme = null) ->
        super(factory, csstheme)

    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if csstheme == null
            csstheme =
                css_links: null
        ###if @position == null
           @position =
              x: 100,
              y:100###
        if (@joint == null) || (@joint.length < @classes.length)
            @joint = []
            @classes.forEach( (elt, index, arr) ->
                if (!this.has_joint_instance(elt))
                    if index == 0
                        # Only draw the "disjoint-covering" label at the first line.
                        disjoint = @disjoint
                        covering = @covering
                    else
                        disjoint = covering = false
                    @targetCells.push(elt.get_classid())
            this)
            pos1 = graph.getCell(@parent_class.get_classid()).get("position")
            pos2 = graph.getCell(@targetCells[0]).get("position")
            if @position == null
              @position =
                x:(pos1.x+pos2.x)/2,
                y:(pos1.y+pos2.y)/2
            @joint.push(factory.create_generalization(
                @parent_class.get_classid(),
                @targetCells,
                csstheme.css_links
                @disjoint,
                @covering,
                @position))


    # Has the given elt a JointJS::Cell insance already created?
    #
    # @param elt {Class} a Class instance.
    # @return true if elt has a joint instance at the @joint variable. false otherwise.
    has_joint_instance: (elt) ->
        classid = elt.get_classid()
        founded = @joint.find( (elt, index, arr) ->
            # elt is a JointJS::Cell instance, a UML::Generalization if the UMLFactory is used.
            elt.get('source').id == classid
        )
        return founded?

    # Search for a child JointJS::Cell inside this relation.
    #
    # @return undefined or null if not founded.
    get_joint_for_child: (classchild) ->
        classid = classchild.get_classid()
        founded = @joint.find( (elt, index, arr) ->
            # elt is a JointJS::Cell instance, a UML::Generalization if the UMLFactory is used.
            elt.get('source').id == classid
        )
        return founded


    has_parent: (parentclass) ->
        return @parent_class == parentclass

    # Add a child into the generalization.
    #
    # @param childclass {Class} a Class instance to add.
    add_child: (childclass) ->
        @classes.push(childclass)

    set_disjoint: (@disjoint) ->
    set_covering: (@covering) ->

    get_disjoint: () ->
        return @disjoint
    get_covering: () ->
        return @covering

    has_link: (link) ->
      a = link.get_gen()
      b = link.get_class()

      if (@classes.includes(b) or @parent_class == b) and this == a
        return true
      else return false

    # This function removes only child classes
    #
    # @todo refactoring to allow removing parent classes
    remove_class_from_gen: (link) ->
      c = link.get_class()

      if @parent_class == c
        @parent_class = []
      else
        @classes = @classes.filter( (elt, index, arr) ->
            elt != c
            )

    to_json: () ->
        json = super()
        json.parent = @parent_class.get_fullname()
        json.classes = $.map(@classes, (myclass) ->
            myclass.get_fullname()
        )
        json.multiplicity = null
        json.roles = null
        json.type = "generalization"

        constraint = []
        if @disjoint then constraint.push("disjoint")
        if @covering then constraint.push("covering")
        json.constraint = constraint
        if @joint?
          json.position = @joint[0].position()
        return json


Generalization.get_new_name = () ->
    if Generalization.name_number == undefined
       Generalization.name_number = 0
    Generalization.name_number = Generalization.name_number + 1
    return "s" + Generalization.name_number

exports.model.uml.Generalization = Generalization
