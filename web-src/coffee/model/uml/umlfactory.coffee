# umlfactory.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

uml = joint.shapes.uml

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# UML Factory for creating JointJS shapes representing a primitive in
# its plugins.
#
# @namespace model.uml
class UMLFactory extends model.Factory

    constructor: () ->
        super()

    #     @param [hash] css_class A CSS class definition in a
    #     Javascript hash. See the JointJS documentation and demos.
    #
    # @return [joint.shapes.uml.Class]
    create_class: (class_name, attribs, methods, css_class=null) ->
        params =
            #position: {x: 20, y: 20}
            size: {width: 120, height: 90}
            name: class_name
            attributes: attribs
            methods: methods
            attrs:
                '.uml-class-name-rect':
                    fill: '#d6d6d6'
                    stroke: '#ffffff'
                '.uml-class-name-text':
                    fill: '#000000'
        if css_class?
            params.attrs = css_class

        newclass = new uml.Class( params )

        return newclass

    # Create association links.
    #
    # @param mult [array] The multiplicity strings.
    # @param roles [array] An array of two strings with the roles names.
    #
    # @return [joint.dia.Link]
    #Creates a link from the AssociationIcon
    create_from_association_link: (class_a_id, class_b_id, name = null, css_links = null, mult = null, u_role = null, d_role = null) ->
        link = new joint.shapes.uml.Association(
            source: {id: class_a_id}
            target: {id: class_b_id}
            #attrs: css_links
        )
        # Format the strings for the labels.
        str_labels = [null, null]
        if u_role isnt null
            str_labels[0] = u_role
        else
            str_labels[0] = d_role

        if mult isnt null && mult isnt "0..*"
            if str_labels[0] isnt null
                str_labels[0] += "\n" + mult
            else
                str_labels[0] = mult
        str_labels[1] = name

        # Create the labels objects.
        labels = []

        if str_labels[0] isnt null
            labels[0]=
                position: 0.75,
                attrs:
                    text: {text: str_labels[0], fill: '#000000','font-size':12},
                    rect: {fill: '#ffffff'}

        link.set({labels: labels})
        return link

    #Creates a link to the AssociationIcon
    create_to_association_link: (class_a_id, class_b_id, name = null, css_links = null, mult = null, role = null) ->
        link = new joint.shapes.uml.Association(
            source: {id: class_a_id}
            target: {id: class_b_id}
            #attrs: css_links
        )

        m = ""

        #Format the strings for the labels.
        str_labels = [null, null]
        str_labels[0] = role

        if mult isnt null && mult isnt "0..*"
            if str_labels[0] isnt null
                str_labels[0] += "\n" + mult
            else
                str_labels[0] = mult

        str_labels[1] = name

        # Create the labels objects.
        labels = []
        if str_labels[0] isnt null
            labels[0] =
                position: 0.25,
                attrs:
                  text: { text: str_labels[0], fill: '#000000', 'font-size':12},
                  rect: {fill: '#ffffff'}

        link.set({labels: labels})
        return link


    #Creates an AssociationIcon
    create_nary_association: (class_a_id, class_b_id, name = null, css_links = null, mult = null, roles = null, position = null) ->
        link = new joint.shapes.uml.AssociationIcon(
            source: {id: class_a_id}
            target: class_b_id
            attrs: {
              text: {
                text: name
              }}
            size: {width: 50, height: 50}
            position: {x:position.x, y: position.y}
        )
        return link

    create_binary_association: (class_a_id, class_b_id, name = null, css_links = null, mult_from = null, mult_to = null) ->
        link = new joint.shapes.uml.Association(
            source: {id: class_a_id}
            target: {id: class_b_id}
            #attrs: css_links
        )

        # Format the strings for the labels.
        # str_labels[0] -> mult_from
        # str_labels[1] -> name
        # str_labels[2] -> mult_to
        str_labels = [null, null, null]

        if mult_from isnt null and mult_from isnt undefined and mult_from isnt ""
          if str_labels[0] isnt null
            str_labels[0] += "\n" + mult_from
          else
            str_labels[0] = mult_from

        if mult_to isnt null and mult_to isnt undefined and mult_from isnt ""
          if str_labels[2] isnt null
            str_labels[2] += "\n" + mult_to
          else
            str_labels[2] = mult_to

        str_labels[1] = name

        # Create the labels objects.

        labels = []

        # name
        if str_labels[1] isnt null
            labels.push(
                position: 0.5
                attrs:
                    text: {text: str_labels[1], fill: '#000000'}
                    rect: {fill: '#ffffff'})

        # from and to association roles and mult
        if str_labels[0] isnt null
          if str_labels[0] is "0..*"
            labels.push(
                position: 0.2,
                attrs:
                    text: {text: null, fill: '#000000'},
                    rect: {fill: '#ffffff'})
          else
            labels.push(
                position: 0.2,
                attrs:
                    text: {text: str_labels[0], fill: '#000000'},
                    rect: {fill: '#ffffff'})

        if str_labels[2] isnt null
          if str_labels[2] is "0..*"
            labels.push(
                position: 0.8,
                attrs:
                    text: {text: null, fill: '#000000'},
                    rect: {fill: '#ffffff'})
          else
            labels.push(
                position: 0.8,
                attrs:
                    text: {text: str_labels[2], fill: '#000000'},
                    rect: {fill: '#ffffff'})

        link.set({labels: labels})
        return link


    # @param css_links {Hash} A Hash representing the CSS. See JointJS documentation for the attrs attribute.
    # @param disjoint {Boolean} Draw a "disjoint" legend.
    # @param covering {Boolean} Draw a "covering" legend.
    # @return [joint.shapes.uml.Generalization]
    create_to_generalization_link: (class_a_id, class_b_id, css_links = null, disjoint = false, covering = false) ->
        labels = []

        link = new uml.Generalization(
            source: {id: class_b_id}
            target: {id: class_a_id}
        )

        return link

    create_generalization: (class_a_id, class_b_id, css_links = null, disjoint = false, covering = false, position = null) ->
        labels = []

        link = new joint.shapes.uml.GeneralizationIcon(
          source: {id: class_b_id}
          target: class_a_id
          attrs: css_links
          position: {x:position.x, y: position.y}
          )

        if disjoint || covering
            legend = "{"
            if disjoint then legend = legend + "disjoint"
            if covering
                if legend != ""
                    legend = legend + ","
                legend = legend + "covering"
            legend = legend + "}"

        labels = labels.concat([
            position: 0.8
            attrs:
                text:
                    text: legend
                    fill: '#0000ff'
                rect:
                    fill: '#ffffff'

        ])
        link.set({labels: labels})

        return link

    create_from_generalization_link: (class_a_id, class_b_id, css_links = null, disjoint=false, covering=false) ->
        link = new joint.shapes.uml.GeneralizationChildLink(
          source: {id: class_b_id}
          target: {id: class_a_id}
          )

        return link


    # Create an association class.
    #
    #
    # @see #create_class
    create_association_class: (class_name, css_class = null) ->
      return this.create_class(class_name, css_class)

    # Create an association link only (the one dashed one that appears between
    # the UML association and the UML association class).
    #
    # @return [joint.dia.Link] a Joint Link object.
    create_linkwithclass: (class_a_id, class_b_id, name = null, css_links = null, mult = null, roles = null) ->
      link = new joint.shapes.uml.LinkWithClass
        source: {
          id: class_a_id
        }
        target: {
          id: class_b_id
        }

      return link



exports.model.uml.UMLFactory = UMLFactory
