# generalization.coffee --
# Copyright (C) 2017 Giménez, Christian, Angela Oyarzun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A link from generalizationIcon.
#
# @namespace model.uml
class LinkFromGeneralization extends model.Link

    # @param parent_class {Class} The parent class.
    # @param classes {Class} Child class.
    constructor: (classes, name = null) ->
        super(classes, name)

    get_class: () ->
      return @classes[1]

    get_gen: () ->
      return @classes[0]

    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if csstheme == null
            csstheme =
                css_links: null
        if @joint == null
            @joint = []
            @joint.push(factory.create_from_generalization_link(
                @classes[0].get_relid(),
                @classes[1].get_classid(),
                @name,
                @disjoint,
                @covering))

      to_json: () ->

exports.model.uml.LinkFromGeneralization = LinkFromGeneralization
