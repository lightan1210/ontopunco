# eerdiagram.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# {ERDFactory} = require './factories'

exports = exports ? this
exports.model = exports.model ? {}
exports.model.eer = exports.model.eer ? {}

#
# An ER diagram representation.
#
# @namespace model.eer
class ERDiagram extends model.Diagram
    #
    # @param [joint.Graph] graph
    #
    constructor: (@graph = null) ->
        super()
        @clases = []
        @attributes = []
        @isa = []
        @relationships = []
        @links = []

        @cells_nuevas = []
        # Cells that are listed for deletion, you have to update
        # diagram for apply.
        @cells_deleted = []

        @factory = new model.eer.ERDFactory()

    get_factory: () ->
        return @factory

    set_factory: (@factory) ->

    get_graph: () ->
        return @graph

    set_graph: (@graph) ->

    get_clases: () ->
        return @clases

    get_links: () ->
        return @links

    get_attributes: () ->
        return @attributes

    get_isa: () ->
        return @isa

    get_relationships: () ->
        return @relationships

    get_last_isa_by_id: () ->
        return @isa[@isa.length - 1].get_isaid()

    get_last_rel_by_id: () ->
        return @relationships[@relationships.length - 1].get_relid()

    # @todo temporal code
    get_mult_from: (relid) ->
        if @relationships[@relationships.length - 1].get_relid() == relid
          return @relationships[@relationships.length - 1].mult[0]

    get_mult_to: (relid) ->
        if @relationships[@relationships.length - 1].get_relid() == relid
          return @relationships[@relationships.length - 1].mult[1]

    get_role_from: (relid) ->
        if @relationships[@relationships.length - 1].get_relid() == relid
          return @relationships[@relationships.length - 1].roles[0]

    get_role_to: (relid) ->
        if @relationships[@relationships.length - 1].get_relid() == relid
          return @relationships[@relationships.length - 1].roles[1]

    get_clase: (nombre) ->

    find_class_by_name: (name) ->
        return @clases.find( (elt, index, arr) ->
            elt.get_name() == name
        )

    find_class_by_classid: (classid) ->
        return @clases.find( (elt,index,arr) ->
            elt.has_classid(classid)
        )

    find_attr_by_name: (name) ->
        return @attributes.find( (elt, index, arr) ->
            elt.get_name() == name
        )

    find_attr_by_attrid: (attrid) ->
        return @attributes.find( (elt,index,arr) ->
            elt.has_attrid(attrid)
        )

    find_isa_by_name: (name) ->
        return @isa.find( (elt, index, arr) ->
            elt.get_name() == name
        )

    find_isa_by_isaid: (isaid) ->
        return @isa.find( (elt,index,arr) ->
            elt.has_isaid(isaid)
        )

    find_rel_by_name: (name) ->
        return @relationships.find( (elt, index, arr) ->
            elt.get_name() == name
        )

    find_rel_by_relid: (relid) ->
        return @relationships.find( (elt,index,arr) ->
            elt.has_relid(relid)
        )


    # Find a generalization that contains the given parent
    #
    # @param parentclass {Class} A Class instance that is the parent of the
    #     generalization.
    # @return null if nothing founded, a Generalization instance otherwise.
    find_IsA_with_parent: (parentclass) ->
        return @links.find( (elt, index, arr) ->
            elt.has_parent(parentclass)
        )

    # @param linkid {String}
    # @return {Link} The MyModel's link.
    find_link_by_source_and_target: (name,class_a,class_b) ->
        @links.find( (elt, index, arr) ->
            elt.hasSourceAndTarget(name,class_a,class_b)
        )

    # Add a Generalization link.
    #
    # If a generalziation already exists for the same parent, just add the class
    # into the same Generalizatino instance. Constraints are ignored in this case.
    #
    # This methods try to normalize all parameters  and then call add_generalization_objs().
    #
    # @param class_parent {string, object} The parent class Id string or the class_parent Class object.
    # @param class_child_id {string, array of strings, array of objects, object} The child class string, an array of class Ids strings, an array of Class objects or the child object.
    #
    #add_generalization: (class_parent, class_childs, disjoint=false, covering=false) ->
    #    class_parent_obj = null
    #    class_child_obj = null

        # Normalize class_parent
    #    if typeof(class_parent) == "string"
            # class_parent is an Id string
    #        class_parent_obj = this.find_class_by_classid(class_parent)
    #    else if typeof(class_parent) == "object"
            # class_parent is the Class instance
    #        class_parent_obj = class_parent

        # Normalize class_childs
    #    if class_childs instanceof Array
            # class_child is an Array, add one by one...
    #        class_childs.forEach( (child) ->
                # child could be a string or obj, it doesn't matter! :-)
    #            this.add_generalization(class_parent, child, disjoint, covering)
    #        this)
    #    else if typeof(class_childs) == "string"
            # class_child is an Id string
    #        class_child_obj = this.find_class_by_classid(class_childs)
    #    else if typeof(class_childs) == "object"
            # class_child is the Class instance
    #        class_child_obj = class_childs

    #    if class_child_obj? and class_parent_obj?
    #        this.add_generalization_objs(class_parent_obj, class_child_obj, disjoint, covering)
#####ORIGINAL
    # Add a Generalization link.
    #
    # If a generalziation already exists for the same parent, just add the class
    # into the same Generalization instance. Constraints are ignored in this case.
    #
    # This methods try to normalize all parameters  and then call add_generalization_objs().
    #
    # @param class_parent {string, object} The parent class Id string or the class_parent Class object.
    # @param class_child_id {string, array of strings, array of objects, object} The child class string, an array of class Ids strings, an array of Class objects or the child object.
    #
    add_generalization: (class_parent, class_childs, disjoint=false, covering=false,total=false,name = null, position=null) ->
        class_parent_obj = null
        class_child_obj = null
        classes_child_objs = []

        # Normalize class_parent
        if typeof(class_parent) == "string"
            # class_parent is an Id string
            class_parent_obj = this.find_class_by_classid(class_parent)
        else if typeof(class_parent) == "object"
            # class_parent is the Class instance
            class_parent_obj = class_parent

        # Normalize class_childs
        if class_childs instanceof Array
          i=0
          while i<class_childs.length
            if typeof(class_childs[i]) == "string"
                # class_child is an Id string
                class_child_obj = this.find_class_by_classid(class_childs[i])
            else if typeof(class_childs[i]) == "object"
                # class_child is the Class instance
                class_child_obj = class_childs[i]
            classes_child_objs.push(class_child_obj)
            i=i+1
          if class_parent_obj == undefined
             class_parent_obj=class_parent
          this.add_generalization_objs(class_parent_obj, classes_child_objs, disjoint, covering,total, name,position)
        else
          if typeof(class_childs) == "string"
            # class_child is an Id string
            class_child_obj = this.find_class_by_classid(class_childs)
          else
            if typeof(class_childs) == "object"
              # class_child is the Class instance
              class_child_obj = class_childs
          if class_child_obj? and class_parent_obj?
            this.add_generalization_objs(class_parent_obj, class_child_obj, disjoint, covering,total, name,position)

    # Add a Generalization link.
    #
    # If a generalziation already exists for the same parent, just add the class
    # into the same Generalizatino instance. Constraints are ignored in this case.
    #
    # @param class_parent {Class instance} A Class object.
    # @param class_child {Class instance} A Class object.
    #
    #add_generalization_objs: (class_parent, class_child, disjoint=false, covering=false) ->
    #    gen = this.find_IsA_with_parent(class_parent)
    #    if (gen is undefined) || (gen is null)
    #        gen = new model.eer.Isa(class_parent, [class_child])
    #        gen.set_disjoint(disjoint)
    #        gen.set_covering(covering)
    #        this.agregar_isa(gen)
    #    else
    #        gen.add_child(class_child)
    #        gen.create_joint(@factory, csstheme)
    #        @cells_nuevas.push(gen.get_joint_for_child(class_child))
    #        this.actualizar_graph()
####ORIGINAL
    # Add a Generalization link.
    #
    # If a generalziation already exists for the same parent, just add the class
    # into the same Generalizatino instance. Constraints are ignored in this case.
    #
    # @param class_parent {Class/Isa instance} A Class/Isa object.
    # @param class_child {Class instance} A Class object.
    #
    add_generalization_objs: (class_parent, class_child, disjoint=false, covering=false, total=false,name = null,position=null) ->
        gen = this.find_isa_by_isaid(class_parent)
        if (gen is undefined) || (gen is null)
            gen = new model.eer.Isa(class_parent, class_child, name,position)
            gen.set_disjoint(disjoint)
            gen.set_covering(covering)
            gen.set_total(total)
            this.agregar_isa(gen)
            this.agregar_link(gen)
            this.add_relationship_isa(class_parent, gen, name,total)
            class_child.forEach( (child) ->
                       this.add_relationship_isa_inverse(gen, child, name)
                   this)
        else
            class_child.forEach( (child) ->
                   gen.add_child(child)
                   this.add_relationship_isa_inverse(gen, child, name)
               this)


    agregar_isa: (gen) ->
        @isa.push(gen)
        @cells_nuevas.push(gen.get_joint(@factory,csstheme))
        this.actualizar_graph()


    add_rel: (rel) ->
      @relationships.push(rel)
      @cells_nuevas.push(rel.get_joint(@factory,csstheme))
      this.actualizar_graph()


    # @param class_a_id {string} the ID of the first class.
    # @param class_b_id {string} the ID of the second class.
    # @param name {string} optional. The name of the association.
    # @param mult {array} optional. An array of two strings with the cardinality from class a and to class b.
    # @param roles {array} optional. An array of two strings with the from and to roles.
    ###add_association: (class_a_id, class_b_id, name = null, mult = null, roles = null) ->
        class_a = this.find_class_by_classid(class_a_id)
        class_b = this.find_class_by_classid(class_b_id)

        newrel = new model.eer.Relationship([class_a, class_b], name, mult, roles)
        if (mult isnt null)
          newrel.set_mult(mult)
        if (roles isnt null)
          newrel.set_roles(roles)

        this.add_rel(newrel)

        rel_id = @get_last_rel_by_id()

        mult_from = @get_mult_from(rel_id)
        role_from = @get_role_from(rel_id)

        newlinktoRel_from = new model.eer.LinkRelToEntity([class_a, newrel], role_from)
        if (mult_from isnt null)
            newlinktoRel_from.set_mult(mult_from)
        if (role_from isnt null)
            newlinktoRel_from.set_roles(role_from)

        this.agregar_link(newlinktoRel_from)

        mult_to = @get_mult_to(rel_id)
        role_to = @get_role_to(rel_id)

        newlinktoRel_to = new model.eer.LinkRelToEntity([class_b, newrel], role_to)
        if (mult_to isnt null)
            newlinktoRel_to.set_mult(mult_to)
        if (role_to isnt null)
            newlinktoRel_to.set_roles(role_to)

        this.agregar_link(newlinktoRel_to)###
    # @param class_a_id {string} the ID of the class/relationshipIcon that started the association.
    # @param class_b_id {Array<string>} the ID of the other classes/attributes involved in the association.
    # @param name {string} optional. The name of the association.
    # @param mult {array} optional. An array of strings with the cardinalities.
    # @param roles {array} optional. An array of strings with the roles.
    add_association: (class_a_id, class_b_id, name = null, mult = null, roles = null,position=null) ->
        assoc=this.find_rel_by_relid(class_a_id)
        if assoc == null or assoc ==undefined
          class_a = this.find_class_by_classid(class_a_id)
          classes = []
          #attributes=[]
          i=0
          while i < class_b_id.length
            class_b = this.find_class_by_classid(class_b_id[i])
            if class_b != null and class_b != undefined
              classes.push(class_b)
            ###else
              attr_b = this.find_attr_by_attrid(class_b_id[i])
              if attr_b != null and attr_b != undefined
                attributes.push(attr_b)###
            i=i+1
          #Add relationshipIcon
          assocIcon=new model.eer.Relationship([class_a, classes], name[0],position)
          if (mult isnt null)
              assocIcon.set_mult(mult)
          #assocIcon.set_attributes(attributes)
          #  if (roles isnt null)
          #      assocIcon.set_roles(roles)
          this.agregar_link(assocIcon)
          this.add_rel(assocIcon)
          assoc_name=assocIcon.get_name()
          #Add link between entity and relationshipIcon
          toIcon = new model.eer.LinkEntityToRel([class_a, assocIcon], assoc_name,mult[0],[roles[0],class_a.name])
          assocIcon.set_roles([roles[0],class_a.name])
          this.agregar_link(toIcon)
          #Add link between relationshipIcon and entity
          i=0
          while i < classes.length
              fromIcon = new model.eer.LinkRelToEntity([assocIcon, classes[i]], assoc_name,mult[i+1],[roles[i+1],classes[i].name])
              assocIcon.set_roles([roles[i+1],classes[i].name])
              this.agregar_link(fromIcon)
              i=i+1

          ###Add link between relationshipIcon and attribute
          i=0
          while i < attributes.length
              fromIcon = new model.eer.LinkRelToAttr([assocIcon, attributes[i]], assoc_name)
              this.agregar_link(fromIcon)
              i=i+1###
        else
          class_b=this.find_class_by_classid(class_b_id[0])
          if class_b != null and class_b != undefined
            #Delete link between relationshipIcon and entity if it exists and
            #add new one with different roles and multiplicity
            if assoc.get_from().name!=class_b.name
              deleted=this.delete_link_by_source_and_target(assoc.get_name(),assoc,class_b)
              link = new model.eer.LinkRelToEntity([assoc, class_b], assoc.get_name(),mult[1],[roles[1],class_b.name])
            else
              deleted=this.delete_link_by_source_and_target(assoc.get_name(),class_b,assoc)
              link = new model.eer.LinkEntityToRel([class_b,assoc], assoc.get_name(),mult[1],[roles[1],class_b.name])
            if deleted
              assoc.set_class_mult(class_b.name, mult[1])
              assoc.set_class_role([roles[1],class_b.name])
            else
              #Add link between relationshipIcon and entity
              assoc.add_class(class_b, mult[1],[roles[1],class_b.name])
            this.agregar_link(link)
          ###else
            #Add link between relationshipIcon and attribute
            attr_b = this.find_attr_by_attrid(class_b_id[0])
            if attr_b != null and attr_b != undefined
              assoc.add_attr(attr_b)
              fromIcon = new model.eer.LinkRelToAttr([assoc, attr_b],assoc.get_name())
              this.agregar_link(fromIcon)###



    add_association_class: (class_a_id, class_b_id, name, mult = null, roles= null) ->
        class_a = this.find_class_by_classid(class_a_id)
        class_b = this.find_class_by_classid(class_b_id)

        newassoc = new model.eer.LinkWithClass([class_a, class_b], name)
        if (mult isnt null)
            newassoc.set_mult(mult)
        if (roles isnt null)
            newassoc.set_roles(roles)

        this.agregar_link(newassoc)
        newassoc.update_position()

    #
    # # Hash Data
    #
    # * `name`    (mandatory)
    # * `attribs` (optional)
    # * `methods` (optional)
    #
    # @example Adding a class
    #   diagram_instance.add_class({name: "class A"})
    #   diagram_instance.add_class({name: "class B", ["attrib1", "attrib2"], ["method1", "method2"]})
    #
    #
    # @param hash_data {Hash} data information for creating the new {Class} instance.
    # @return The new class
    # @see Class
    # @see GUI#add_class
    add_entity: (hash_data) ->
        if hash_data.attrs == undefined
            hash_data.attrs = []
        if hash_data.position == undefined
            hash_data.position =
              x: 20,
              y: 20
        newclass = new model.eer.Entity(hash_data.name, hash_data.attrs,hash_data.position)
        this.agregar_clase(newclass)
        return newclass

    agregar_clase: (clase) ->
        @clases.push(clase)
        @cells_nuevas.push(clase.get_joint(@factory, csstheme))
        this.actualizar_graph()


    add_attribute: (hash_data) ->
        newattribute = new model.eer.Attribute(hash_data.name, hash_data.type)
        this.agregar_attribute(newattribute)
        return newattribute

    agregar_attribute: (attribute) ->
        @attributes.push(attribute)
        @cells_nuevas.push(attribute.get_joint(@factory,csstheme))
        this.actualizar_graph()


    add_relationship_attr_entity: (class_id, attribute_id, name) ->
        entity = this.find_class_by_classid(class_id)
        attr = this.find_attr_by_attrid(attribute_id)
        newrel = new model.eer.LinkAttrToEntity([entity, attr], name)
        this.agregar_link(newrel)

    add_relationship_rel_attr: (rel_id, attribute_id, name) ->
        assocIcon = this.find_rel_by_relid(rel_id)
        attr = this.find_attr_by_attrid(attribute_id)
        fromIcon = new model.eer.LinkRelToAttr([assocIcon, attr], name)
        assocIcon.add_attribute(attr)
        this.agregar_link(fromIcon)

    add_relationship_isa: (entity, isa, name,total) ->
        #entity = this.find_class_by_classid(class_id)
        #isa = this.find_isa_by_isaid(isa_id)
        newlinktoISA = new model.eer.LinkISAToEntity([entity, isa], name,"direct",total)
        this.agregar_link(newlinktoISA)

    add_relationship_isa_inverse: (isa, entity, name) ->
        #entity = this.find_class_by_classid(class_id)
        #isa = this.find_isa_by_isaid(isa_id)
        newlinkfromISA = new model.eer.LinkISAToEntity([isa, entity], name,"inverse")
        this.agregar_link(newlinkfromISA)

    add_relationship_rel: (class_id, rel_id, name, mult, roles) ->
        entity = this.find_class_by_classid(class_id)
        rel = this.find_rel_by_relid(rel_id)
        newlinktoRel = new model.eer.LinkRelToEntity([entity, rel], name)
        if (mult isnt null)
            newlinktoRel.set_mult(mult)
        if (roles isnt null)
            newlinktoRel.set_roles(roles)

        this.agregar_link(newlinktoRel)

    add_relationship_rel_inverse: (rel_id, class_id, name, mult, roles) ->
        entity = this.find_class_by_classid(class_id)
        rel = this.find_rel_by_relid(rel_id)
        newlinkfromRel = new model.eer.LinkRelToEntity([rel, entity], name)
        if (mult isnt null)
            newlinkfromRel.set_mult(mult)
        if (roles isnt null)
            newlinkfromRel.set_roles(roles)

        this.agregar_link(newlinkfromRel)

    # @param c {Class instance}.
    delete_class: (c) ->
        @clases = @clases.filter( (elt, index, arr) ->
            elt != c
        )

        this.remove_associated_links(c)

        @cells_deleted = @cells_deleted.concat(c.get_joint())
        this.actualizar_graph()


    delete_attribute: (c) ->
        @attributes = @attributes.filter( (elt, index, arr) ->
            elt != c
        )

        this.remove_associated_links(c)

        @cells_deleted = @cells_deleted.concat(c.get_joint())
        this.actualizar_graph()

    # @param c {Isa instance}.
    delete_isa: (c) ->
        @isa = @isa.filter( (elt, index, arr) ->
            elt != c
        )
        this.remove_associated_links(c)
        @cells_deleted = @cells_deleted.concat(c.get_joint())
        this.actualizar_graph()

    delete_isa_by_isaid: (isaid) ->
        isa = this.find_isa_by_isaid(isaid)
        if isa?
            this.delete_isa(isa)

    # @param c {Relationship instance}.
    delete_relationship: (c) ->
        @relationships = @relationships.filter( (elt, index, arr) ->
            elt != c
        )
        this.remove_associated_links(c)
        @cells_deleted = @cells_deleted.concat(c.get_joint())
        this.actualizar_graph()

    delete_rel_by_relid: (relid) ->
        rel = this.find_rel_by_relid(relid)
        if rel?
            this.delete_relationship(rel)

    # Search for all links associated to the given class.
    #
    # @param c {Class instance} The class.
    #
    # @return Array of Links instances.
    find_associated_links: (c) ->
        @links.filter( (link, indx, arr) ->
            link.is_associated(c)
        this)


    # Remove all links associated to the given class.
    #
    # @param c {Class instance} The class.
    remove_associated_links: (c) ->
        lst = this.find_associated_links(c)
        lst.forEach( (link, indx, arr) ->
            this.delete_link(link)
        this)

    rename_class: (classid, name) ->
        c = this.find_class_by_classid(classid)
        if c != null
            c.set_name(name)

    rename_attribute: (attrid, name) ->
        c = this.find_attr_by_attrid(attrid)
        if c != null
            c.set_name(name)

    # Remove the given link from the diagram.
    #
    # @param link {Link instance} The link to remove.
    delete_link: (link) ->
        @links = @links.filter( (elt, index, arr) ->
            elt != link
        )

        @cells_deleted = @cells_deleted.concat(link.get_joint())
        this.actualizar_graph()


    # Update the view associated to the given class/attribute's class/attribute id if it
    # have already created its joint object. The view to be updated
    # must be of the given
    update_view: (class_id, paper) ->
        class_obj = this.find_class_by_classid(class_id)
        attr_obj = this.find_attr_by_attrid(class_id)
        if class_obj != undefined
            class_obj.update_view(paper)
        if attr_obj != undefined
            attr_obj.update_view(paper)


    # Remove the link by its source and target from the diagram.
    #
    # @param name,class_a,class_b are strings
    delete_link_by_source_and_target: (name,class_a,class_b) ->
        deleted=false
        link = this.find_link_by_source_and_target(name,class_a,class_b)
        console.log('LINK FOUND ', link)
        if link?
            this.delete_link(link)
            deleted=true
        return deleted

    # Remove the class from the diagram.
    delete_class_by_name: (name) ->
        c = this.find_class_by_name(name)
        if c != undefined then this.delete_class(c)


    # Delete a class selecting by using its Joint Model
    #
    # @param [classid] string.
    delete_class_by_classid: (classid) ->
        c = this.find_class_by_classid(classid)
        if c != undefined then this.delete_class(c)

    # Delete a class selecting by using its Joint Model
    #
    # @param [classid] string.
    delete_attr_by_attrid: (attrid) ->
        c = this.find_attr_by_attrid(attrid)
        if c != undefined then this.delete_attribute(c)


    # Reset the current diagram to start over empty.
    #
    # Remove all classes and associations.
    reset: () ->
        # Associations is supposed to be deleted after each classes has
        @clases.forEach( (c, i, arr) ->
            this.delete_class(c)
        this)
        @attributes.forEach( (a, i, arr) ->
            this.delete_attribute(a)
        this)
        @links.forEach( (l, i, arr) ->
            this.delete_link(l)
        this)
        this.actualizar_graph()


    # # Limitations
    # If the link is a generalization it adds as a new generalization,
    # not as part of another one.
    #
    # For example, if you want to add another child into an already existent
    # generalization, use add_generalization(same_parent, new_child) message.
    agregar_link: (link) ->
        @links.push(link)
        @cells_nuevas.push(link.get_joint(@factory, csstheme));
        this.actualizar_graph()

    # Show these classes as satisifiable.
    #
    # @see set_class_satisfiable
    #
    # @param classes_list {Array<String>} a list of classes names.
    set_satisfiable: (classes_list) ->
        classes_list.forEach( (class_name, i, arr) ->
            this.set_class_satisfiable(class_name)
        this)

    # Show this class as satisfiable.
    #
    # @param class_name {String} The class name.
    set_class_satisfiable: (class_name) ->
        c = this.find_class_by_name(class_name)
        if c?
            c.set_unsatisfiable(false, csstheme)

    # Show these classes as unsatisifiable.
    #
    # @see set_class_unsatisfiable
    #
    # @param classes_list {Array<String>} a list of classes names.
    set_unsatisfiable: (classes_list) ->
        classes_list.forEach( (class_name, i, arr) ->
            this.set_class_unsatisfiable(class_name)
        this)

    # Show this class as unsatisfiable.
    #
    # @param class_name {String} The class name.
    set_class_unsatisfiable: (class_name) ->
        c = this.find_class_by_name(class_name)
        if c?
            c.set_unsatisfiable(true, csstheme)

    # Update a joint.Graph instance with the new cells.
    actualizar_graph: () ->
        if @graph != null
            @graph.addCell(@cells_nuevas)
            # remove the removed cells
            @cells_deleted.forEach(
                (elt,index,arr) ->
                    elt.remove()
            )
        @cells_deleted = []
        @cells_nuevas = []


    # Return a JSON representing the Diagram.
    #
    # We want to send only some things not all the JSON the object
    # has. So, we drop some things, like JointJS graph objects.
    #
    # Use JSON.stringify(json_object) to translate a JSON object to
    # string.
    to_json : () ->
        classes_json = $.map(@clases, (myclass) ->
            myclass.to_json()
            )
        attributes_json = $.map(@attributes, (myattr) ->
            myattr.to_json()
            )
        links_json = $.map(@links, (mylink) ->
            mylink.to_json()
            )
        classes: classes_json
        attributes: attributes_json
        links: links_json

    # Import all classes and associations from a JSON object.
    #
    # Make sure to reset() this diagram if you don't want the classes already
    # on this model.
    #
    # @param json {JSON object} a JSON object. Use json = JSON.parse(jsonstr) to retrieve from a string.
    #
    # @todo Better programmed it would be if we pass a JSON part to the constructor of each model class. Leaving the responsability of each MyModel class to create itself.
    import_json: (json) ->
        #attributes
        json.attributes.forEach(
            (elt, index, arr) ->
                c = this.add_attribute(elt)
  #                if c.get_joint()[0].position?
  #                	c.get_joint()[0].position(
  #                    elt.position.x,
  #                    elt.position.y)
        this)

        #classes
        json.classes.forEach((elt, index, arr) ->
                c = this.add_entity(elt)
#                if c.get_joint()[0].position?
#                	c.get_joint()[0].position(
#                    elt.position.x,
#                    elt.position.y)
        this)

        # relationships
        json.links.forEach(
            (elt, index, arr) ->
                if elt.type is "association" or elt.type is "n-ary association without class"
                    class_a = this.find_class_by_name(elt.classes[0])
                    i=1
                    class_b=[]
                    while i<elt.classes.length
                      class_b.push(this.find_class_by_name(elt.classes[i]).get_classid())
                      i=i+1
                    this.add_association(
                          class_a.get_classid(),
                          class_b,
                          [elt.name,],
                          elt.multiplicity,
                          elt.roles,elt.position)
                    if elt.attributes != []
                      i=0
                      relid=this.find_rel_by_name(elt.name).get_relid()
                      while i<elt.attributes.length
                        this.add_relationship_rel_attr(relid,this.find_attr_by_name(elt.attributes[i]).get_attributeid())
                        i=i+1
                if elt.type is "generalization"
                    class_parent = this.find_class_by_name(elt.parent)
                    classes_children = elt.classes.map(
                        (childname) ->
                            this.find_class_by_name(childname)
                    this)
                    disjoint = elt.constraint.includes("disjoint")
                    covering = elt.constraint.includes("covering")
                    total = elt.constraint.includes("total")
                    this.add_generalization(
                        class_parent,
                        classes_children,
                        disjoint, covering,total,elt.name,elt.position)
                if elt.type is "attribute"
                    class_a = this.find_class_by_name(elt.classes[0])
                    attr_a = this.find_attr_by_name(elt.classes[1])
                    this.add_relationship_attr_entity(
                            class_a.get_classid(),
                            attr_a.get_attributeid(),
                            elt.name)

        this)


exports.model.eer.ERDiagram = ERDiagram
