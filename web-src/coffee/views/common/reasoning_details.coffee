exports = exports ? this
exports.views = exports.views ? {}
exports.views.common = exports.views.common ? {}


ReasoningDetailsWidget = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.show()

    render: () ->
        template = _.template( $("#template_reasoning").html() )
        this.$el.html(template({}))


    set_kb: (satisfiable_kb) ->
      $("#reasoner_kb").val("")
      $("#reasoner_kb").val(satisfiable_kb)

    set_reasoner_output: (reasoner_output) ->
      $("#reasoner_output").val("")
      $("#reasoner_output").val(reasoner_output)

    set_sat: (satisfiable) ->
      $("#reasoner_sat").html("")
      $("#reasoner_sat").html(satisfiable)

    set_unsat : (unsatisfiable) ->
      $("#reasoner_unsat").html("")
      $("#reasoner_unsat").html(unsatisfiable)

      # Hide this widget.
      hide: () ->
          this.$el.hide()

      # Show this widget.
      #
      # @param pos {object} Optional. The position, for example: `{x: 10, y: 20}`
      # @param callback_fnc [function] Optional. A callback function without parameters. If not setted, then use the default.
      show: () ->
          this.$el.show()
    )


exports.views.common.ReasoningDetailsWidget = ReasoningDetailsWidget
