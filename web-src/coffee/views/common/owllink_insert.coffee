# owllink_insert.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.common = exports.views.common ? this


##
# A view for inserting and editing OWLlink text.
OWLlinkInsertView = Backbone.View.extend(
    initialize: () ->
        this.render()
        @textarea = this.$el.find("#insert_owllink_input")

    render: () ->
        template = _.template( $("#template_insertowllink").html() )
        this.$el.html( template() )

    events:
        "click button#insert_owlclass" : "insert_class"
        "click button#insert_owldisjoint" : "insert_disjoint"
        "click button#insert_owlequivalence" : "insert_equivalence"

    get_owllink: () ->
        return @textarea[0].value

    set_owllink: (str) ->
        @textarea[0].value = str

    append_owllink: (str) ->
        @textarea[0].value = @textarea[0].value + str

    get_ontologyIRI: () ->
      ontologyIRI = gui.gui_instance.current_gui.diag.ns.get_ontologyIRI_value()

    insert_class: () ->
      class_name = @get_ontologyIRI().concat "CLASSNAME"
      tag = "<owl:Class IRI=\"".concat class_name
      f_tag = tag.concat "\"/>"
      this.append_owllink "<owl:SubClassOf>"
      this.append_owllink f_tag
      this.append_owllink "<owl:Class IRI=\"http://www.w3.org/2002/07/owl#Thing\"/>"
      this.append_owllink "</owl:SubClassOf>"

    insert_disjoint: () ->
      class_name = @get_ontologyIRI().concat "CLASSNAME"
      tag = "<owl:Class IRI=\"".concat class_name
      f_tag = tag.concat "\"/>"
      this.append_owllink "<owl:DisjointClasses>"
      this.append_owllink f_tag
      this.append_owllink f_tag
      this.append_owllink "</owl:DisjointClasses>"

    insert_equivalence: () ->
      class_name = @get_ontologyIRI().concat "CLASSNAME"
      tag = "<owl:Class IRI=\"".concat class_name
      f_tag = tag.concat "\"/>"
      this.append_owllink "<owl:EquivalentClasses>"
      this.append_owllink f_tag
      this.append_owllink f_tag
      this.append_owllink "</owl:EquivalentClasses>"
      
)



exports.views.common.OWLlinkInsertView = OWLlinkInsertView
