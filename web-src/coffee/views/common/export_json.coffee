# export_json.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.common = exports.views.common ? this

ExportJSONView = Backbone.View.extend(
    initialize: () ->
        @jsonstr = ""
        this.render()
    render: () ->
        template = _.template( $("#template_exportjson").html() )
        this.$el.html(template({jsonstr: @jsonstr}))
    events:
        "click button#exportjson_copybtn" : "copy_jsonstr"
        "click button#exportjson_refreshbtn" : "refresh"
    copy_jsonstr: () ->
        $("#exportjson_input").select()
        document.execCommand("Copy")
    refresh: () ->
        gui.gui_instance.widgets.refresh_export_json()
    set_jsonstr: (@jsonstr) ->
        $("#exportjson_input").val(@jsonstr)
)




exports.views.common.ExportJSONView = ExportJSONView
