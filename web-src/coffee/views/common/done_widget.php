<?php
/*

   Copyright 2017 Giménez, Christian

   Author: Giménez, Christian

   done_widget.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="done_widget" style="visible:false, z-index:1, position:absolute">
    <button class="btn btn-primary" type="button" id="done_button">
	Done
    </button>
    <button class="btn btn-danger" type="button" id="cancel_button">
	Cancel
    </button>
</div>
