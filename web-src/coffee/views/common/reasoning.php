
<div class="modal fade" id="reasoning_widget" tabindex="-1" role="dialog"
     aria-labelledby="reasoning_widget" aria-hidden="true">

    <div class="modal-dialog">
	<div class="modal-content">

	    <div class="modal-header">
		<h3 class="modal-title">Reasoner Details</h3>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">

    <h5>KB</h5>
		<textarea class="form-control" cols="40" rows="1"
			  id="reasoner_kb">
    </textarea>

    <h5>Satisfiable</h5>
		<textarea class="form-control" cols="40"
			  id="reasoner_sat">
    </textarea>

    <h5>Unsatisfiable</h5>
		<textarea class="form-control" cols="40"
			  id="reasoner_unsat">
    </textarea>

<!--		<h5>Reasoner Input</h5>
		<textarea class="form-control" cols="40"
			  id="reasoner_input">
    </textarea> -->

    <h5>Reasoner Output</h5>
		<textarea class="form-control" cols="40"
			  id="reasoner_output">
    </textarea>

     </div>

	    <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">
		    Hide
		</button>
	    </div>

	</div>
    </div>

</div>
