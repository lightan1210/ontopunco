<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   exportjson.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="modal fade" tabindex="-1" role="dialog" id="OBDA_widget"
     aria-labelledby="OBDA_widget" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" style="max-width: 95%; max-height: 95%" role="document">
	<div class="modal-content">

	    <div class="modal-header">
		<h1 class="modal-title">OBDA</h1>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="close"><form>
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
	    	Select mappings generator:<br>
			<select class="custom-select" id="mappingsTool">
			    <!--option value="berardi">Diego/Berardi</option-->
			    <option selected="1" value="Automap">Automap</option>
			</select><br>

			Select OBDA framework:<br>
			<select class="custom-select" id="obdaFramework">
			    <!--option value="berardi">Diego/Berardi</option-->
			    <option selected="1" value="Ontop">Ontop</option>
			</select><br>

	    Your database IP: <br><input type="text" id="dbIp" value = "localhost" style="width:100%"><br>
	    Your database username: <br><input type="text" id="dbUsername" style="width:100%"><br>
	    Your database password: <br><input type="password" id="dbPassword" value="" style="width:100%"><br>
	    Your database driver: <br><input type="text" id="dbDriver" value = "org.postgresql.Driver" style="width:100%"><br>
	   	Your database name: <br><input type="text" id="dbName" style="width:100%"><br>
	   	Your database schema: <br><input type="text" id="dbSchema" style="width:100%"><br>

		<p>Insert your SPARQL query here:</p>
		<textarea class="form-control" id="sparql_input"><%= sparqlstr %></textarea>
    </div>
    <div class="col-md-4">
		<p>Mappings (if you want to generate them automatically, skip this step and click "Run FULL OBDA"):</p>
		<textarea class="form-control" id="mappings" style="height:85%"><%= sparqlstr %></textarea>
  </div>
  <div class="col-md-4">
		<p>Your answer (it can take several seconds to get the answer):</p>
		<textarea class="form-control" id="sparql_output" style="height:91%"><%= sparqlstr %></textarea>

	    </div>

	    <div class="modal-footer" style="margin-left: auto">
	    <button id="obda_btn" class="btn btn-secondary">
		Run OBDA
	    </button>
    	<button id="full_obda_btn" class="btn btn-secondary">
		Run FULL OBDA
	    </button>
		<button type="button" class="btn btn-danger"
			data-dismiss="modal">
		    Close
		</button>
	    </div>

</div>

 </div>
	</div>
    </div>
</div>
</div>
</div>