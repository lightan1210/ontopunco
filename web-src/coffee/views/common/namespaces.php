<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   importjson.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="modal fade" id="namespaces_widget" tabindex="-1" role="dialog"
     aria-labelledby="namespaces_widget" aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h3 class="modal-title">Namespaces</h3>
                <button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
		<label>Ontology IRI prefix:</label>
		<div class="input-group">
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="ontoiri_prefix" value=""/>
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="ontoiri_value" value=""/>
		</div>

		<label>Prefixes</label>
		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp1_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv1_input" value="" />
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp2_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv2_input" value="" />
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp3_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv3_input" value="" />
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp4_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv4_input" value="" />
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp5_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv5_input" value="" />
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp6_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv6_input" value="" />
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp7_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv7_input" value="" />
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp8_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv8_input" value="" />
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp9_input" value="" />
		    <input type="text" placeholder="IRI"
			   class="form-control"
			   id="nsv9_input" value="" />
		</div>
            </div>
            <div class="modal-footer">
                <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary"
			    id="accept_namespaces_btn">
			Add
		    </button>
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">
                        Hide
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>
