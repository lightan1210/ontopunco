<?php
/*

   Copyright 2017 Giménez, Christian

   Author: Giménez, Christian

   login.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<div id="loginWidget" class="loginwidget modal" tabindex="-1" role="dialog">

    <div class="modal-dialog" role="document">
	     <div class="modal-content">
	        <div id="login-header" class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		             <span aria-hidden="true">&times;</span>
		          </button>
	    </div>

	    <div class="modal-body">
		  <form>
        <div class="form-group" id="loginForm">
          <div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span>
                        <input class="form-control" type="text" id="login_username" placeholder="username"/>
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
  												<span class="input-group-addon">
  													<i class="glyphicon glyphicon-lock"></i>
  												</span>
                          <input class="form-control" type="password" id="login_password" placeholder="password"/>
          </div>
        </div>
        <div class="form-group">
          <button class="btn btn-lg btn-primary btn-block" type="button" id="login_login_btn">Login</button>
        </div>

		    <div class="form-group" id="logoutForm">
			      <p>Are you sure?</p>
			      <div id="logout_username"></div>
			      <button class="btn btn-danger" type="button" id="logout_logout_btn">Logout</button>
		    </div>
		</form>
	    </div>
	</div>
    </div>
</div>
