<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   insertowllink.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="modal fade" tabindex="-1" role="dialog"
     id="insertowllink_widget"
     aria-labelledby="insertowllink_widget" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">

	    <div class="modal-header">
		<h1 class="modal-title">Insert OWLlink</h1>
		<button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">
		<p>Use this for adding your own personal
		    <a target="_blank"
		       href="http://owllink.org/"> OWLlink</a>
		    data.</p>
		<p>Remember: this will be used when you check for satisfiability or send any diagram information to the server.</p>

		<textarea class="form-control" id="insert_owllink_input"></textarea>

		<div class="btn-group" role="group">
		    <button type="button" id="insert_owlclass" class="btn btn-secondary">
			owl:Class
		    </button>
		</div>

    <div class="btn-group" role="group">
        <button type="button" id="insert_owldisjoint" class="btn btn-secondary">
      owl:DisjointClasses
        </button>
    </div>

    <div class="btn-group" role="group">
        <button type="button" id="insert_owlequivalence" class="btn btn-secondary">
      owl:EquvalentClasses
        </button>
    </div>

	    </div>

	    <div class="modal-footer">
		<button type="button" class="btn btn-primary"
			data-dismiss="modal">Close</button>
	    </div>

	</div>
    </div>

</div>
