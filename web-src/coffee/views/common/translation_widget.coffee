# translation_widget.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.views = exports.views ? {}
exports.views.common = exports.views.common ? this

# @mixin
#
# Widget with a "done" button.
TranslationWidget = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.show()

    render: () ->
        template = _.template( $("#template_translation").html() )
        this.$el.html(template({}))

    events:
        "click button#translate_button" : "translate_again"

    translate_again: () ->
        gui.gui_instance.translate_owllink()

    # Depending on the format, display this text in the widget's
    # textarea or div.
    #
    # @param text [string] The translation text.
    # @param format [string] `html` or `xml` string.
    set_text: (text, format="xml") ->
        if format == "html"
            $("#html-output").html(text)
            $("#html-output").show()
            $("#owllink_source").hide()
        else
            $("#owllink_source").text(text)
            $("#owllink_source").show()
            $("#html-output").hide()


    # Hide this widget.
    hide: () ->
        this.$el.hide()

    # Show this widget.
    #
    # @param pos {object} Optional. The position, for example: `{x: 10, y: 20}`
    # @param callback_fnc [function] Optional. A callback function without parameters. If not setted, then use the default.
    show: () ->
        this.$el.show()
)


exports.views.common.TranslationWidget = TranslationWidget
