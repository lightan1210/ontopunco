<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   editclass.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<form>
    
    <input type="hidden" id="editclass_classid" name="classid" value="<%= classid %>" />
    
    <input class="form-control" type="text" placeholder="Name" id="editname_input" value="" />
    <input class="form-control" placeholder="Prefix" type="text" id="editprefix_input" value="" />
    <input class="form-control" placeholder="URL" type="text" id="editurl_input"  value="" />
    <div class="btn-group" role="group">
	<button class="btn btn-sm btn-primary" type="button" id="uml_editclass_button">
	    Accept
	</button>
	<button class="btn btn-sm btn-danger" type="button" id="uml_close_button">
	    Cancel
	</button>
    </div>
</form>
