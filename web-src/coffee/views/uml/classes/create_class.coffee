# create_class.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.classes = exports.views.uml.classes ? this


# Provides elements and events needed for displaying the interface for
# creating a new class.
CreateClassView = Backbone.View.extend(
        initialize: () ->
            this.render()

        render: () ->
            template = _.template( $("#template_tools_navbar").html(), {} )
            this.$el.html(template)

        events:
            "click a#umlcrearclase_button" :
                "create_class"
            "click a#translate_button1" :
                "translate"
            "click a#insertowllink_button":
                "insert_owllink"
            "click a#resetall_button":
                "reset_all"
            "click a#importjson_open_dialog":
                "import_json"
            "click a#namespaces_open_dialog":
                "set_namespaces"
            "click a#exportjson_open_dialog":
                "export_json"
            "click a#meta_erd_button" : "meta_to_erd"
            "click a#savejson":
                "save_json"
            "click a#loadjson":
                "load_json"
            "click a#importowl_open_dialog" :
                "import_owl"


        meta_to_erd: (event) ->
            console.log(event)
            guiinst.to_erd()


        create_class: (event) ->
            guiinst.diagadapter.add_object_type(
                name: $("#crearclase_input").val()
            )


        # Event handler for translate diagram to OWLlink using Ajax
        # and the api/translate/berardi.php translator URL.
        translate: (event) ->
            syntax = this.get_translation_format()
            strategy = this.get_translation_strategy()
            gui.gui_instance.translate_formal(strategy, syntax)

        # Which is the current translation syntax format selected by the
        # user?
        #
        # @return [String] "html", "owllink", etc.
        get_translation_format: () ->
            $("#format_select")[0].value

        # Which is the current strategy the user selected?
        #
        # @return {String} "berardi", "crowd", etc.
        get_translation_strategy: () ->
            $("#strategy_select")[0].value

        # Which is the current reasoner selected by user?
        #
        # @return {String} "konclude", "racer", etc.
        get_reasoner: () ->
            $("#reasoner")[0].value

        insert_owllink: () ->
            gui.guiinst.show_insert_owllink()
        reset_all: () ->
            gui.guiinst.current_gui.diagadapter.reset_all()
        import_json: () ->
            gui.guiinst.widgets.show_import_json()
        import_owl: () ->
            gui.guiinst.widgets.show_import_owl()
        export_json: () ->
            gui.guiinst.widgets.show_export_json()
        save_json: () ->
            gui.gui_instance.show_save_json()
        load_json: () ->
            gui.gui_instance.show_load_json()
        set_namespaces: () ->
            gui.gui_instance.widget.show_namespaces()

);



exports.views.uml.classes.CreateClassView = CreateClassView
