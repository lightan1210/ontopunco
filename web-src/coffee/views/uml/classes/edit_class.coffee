# edit_class.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.classes = exports.views.uml.classes ? this


EditClassView = Backbone.View.extend
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_editclass").html())
        this.$el.html(template({classid: @classid}))

    events:
        "click button#uml_editclass_button" : "edit_class"
        "click button#uml_close_button" : "hide"

    # Set this class ID and position the form onto the
    #
    # Class diagram.
    set_classid : (@classid) ->

        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

        c = gui.gui_instance.current_gui.diag.find_class_by_classid(@classid)

        $("#editname_input").val(c.get_uri().get_name_uri())
        $("#editprefix_input").val(c.get_uri().get_prefix_uri())
        $("#editurl_input").val(c.get_uri().get_url_uri())
        $("#editurl_input").attr("disabled", "true")
        $("#editurl_input").css("background-color", "lightgray")

    get_classid : () ->
        return @classid

    edit_class: (event) ->
      name = null
      prefix = null
      url = null
      b_name = null

      name = $("#editname_input").val()
      prefix = $("#editprefix_input").val()
      url = $("#editurl_input").val()

      if name != ""
        if prefix != ""
          name_a = prefix.concat ":"
          b_name = name_a.concat name
        else
          b_name = name

      gui.uml.iumladapter.edit_class_name @classid, b_name
      @hide()

    show: () ->
        this.$el.show()

    hide: () ->
        this.$el.hide()


exports.views.uml.classes.EditClassView = EditClassView
