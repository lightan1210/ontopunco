# edit_attributes.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.classes = exports.views.uml.classes ? this

# @mixin
# @namespace views.uml.classes
# @extend Backbone.View
#
# Widget for editing UML class attributes.
#
# Provides options for creating, removing and listing attributes.
#
EditAttributes = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()
      #  @hide()

    render: () ->
#        attrs = ''
#        if @umlclass?
#            attrs = String(@umlclass.attrs)

        template = _.template( $("#template_edit_attributes").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click button#done_button" : "add_attr"
        "click button#uml_close_button" : "hide"


    set_classid: (@classid) ->
        @umlclass = gui.gui_instance.current_gui.diag.find_class_by_classid(@classid)
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        @listenTo(@umlclass, 'change', @render)

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x,
            position: 'absolute',
            'z-index': 1
            )

        $("#umlattr_a_editurl_input").attr("disabled", "true")
        $("#umlattr_a_editurl_input").css("background-color", "lightgray")

    # Display the dialog over the UML class.
    #
    # Need the @classid setted. See set_classid().
    show: () ->
        this.$el.show()

    hide: () ->
        this.$el.hide()

    # Add a new attribute from the text in the input field.
    add_attr: () ->
      name = null
      prefix = null
      url = null
      b_name = null
      type = null

      name = $("input#umlattr_input").val()
      prefix = $("input#umlattr_a_editprefix_input").val()
      url = $("input#umlattr_a_editurl_input").val()
      type = $("select#builtin_datatypes option:selected").text()

      if name != ""
        if prefix != ""
          name_a = prefix.concat ":"
          b_name = name_a.concat name
        else
          b_name = name

      console.log("TYPE SELECTED: ", type)
      console.log("NAME: ", b_name)

      gui.uml.iumladapter.add_attribute @classid, b_name, type
      @hide()

#        attrstr = $("input#umlattr_input").val()
#        @umlclass.add_attr(attrstr)
#        @render()

    # Hide this dialog.
    back: () ->
        @hide()

)

exports.views.uml.classes.EditAttributes = EditAttributes
