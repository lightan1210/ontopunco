
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.generalisation = exports.views.uml.generalisation ? this


GenIconOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_generalisationiconoptions").html() )
        this.$el.html(template({geniconid: @geniconid}))

    events:
        "click button#umldeletegeneralization_button" : "delete_generalization",
        "click button#umladdchild_button" : "edit_generalization",
        "click button#umldisjoint_button" : "edit_disjoint",
        "click button#umlcovering_button" : "edit_covering",
        "click button#metadata_button" : "ns",

    delete_generalization: () ->
      gui.uml.iguiuml.widgets.hide_options()
      gui.uml.iumladapter.delete_class @geniconid

    # Add child to generalization
    edit_generalization: () ->
      gui.uml.iguiuml.widgets.hide_options()
      gui.uml.iguiuml.set_isa_state @geniconid
      gui.state_inst.selectionstate_inst.reset_click()

    edit_covering: () ->
      gui.uml.iguiuml.widgets.hide_options()
      gen = gui.uml.iumladapter.diag.find_generalization_by_id @geniconid
      covering = gen.get_covering()
      gui.uml.iumladapter.edit_covering @geniconid, not covering
      gui.state_inst.selectionstate_inst.reset_click()

    edit_disjoint: () ->
      gui.uml.iguiuml.widgets.hide_options()
      gen = gui.uml.iumladapter.diag.find_generalization_by_id @geniconid
      disjoint = gen.get_disjoint()
      gui.uml.iumladapter.edit_disjoint @geniconid, not disjoint
      gui.state_inst.selectionstate_inst.reset_click()

    set_geniconid: (@geniconid) ->
        viewpos = graph.getCell(@geniconid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

        gen = gui.uml.iumladapter.diag.find_generalization_by_id @geniconid
        covering = gen.get_covering()
        disjoint = gen.get_disjoint()

        if covering
          $("#umlcovering_button").html("Remove Covering")
        else
          $("#umlcovering_button").html("Covering")

        if disjoint
          $("#umldisjoint_button").html("Remove Disjoint")
        else
          $("#umldisjoint_button").html("Disjoint")

    get_geniconid: () ->
        return @geniconid

    ns: (event) ->
      gui.uml.iumlwidgets.hide_options()
      info = gui.gui_instance.current_gui.diagadapter.show_gen_info @geniconid
      gui.uml.iumlwidgets.show_info info
      gui.state_inst.selectionstate_inst.reset_click()

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)

exports.views.uml.generalisation.GenIconOptionsView = GenIconOptionsView
