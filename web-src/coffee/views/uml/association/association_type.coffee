exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this

AssociationTypeView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_association_type").html() )
        this.$el.html(template({classid: @classid}))

    events:
        'click button#assoc_binary_btn' : 'new_binary'
        'click button#assoc_nary_btn' : 'new_nary'
        'click button#uml_close_assoctype_button' : 'hide'

    new_binary: (event) ->
        gui.uml.iumlwidgets.set_editassociation_classid @classid, 'binary'
        @hide()

    new_nary: (event) ->
        gui.uml.iumlwidgets.set_editassociation_classid @classid, 'n-ary'
        @hide()


    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)

exports.views.uml.association.AssociationTypeView = AssociationTypeView
