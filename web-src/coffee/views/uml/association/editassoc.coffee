# editassoc.coffee --
# Copyright (C) 2018 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this


EditAssocView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_editassoc").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click button#done_button" : "edit_assoc"
        "click button#uml_close_button" : "hide"

    # Retrieve the destination role and multiplicity
    cardto: () ->
        name = null
        prefix = null
        too_1 = $('#narycardto-1').val()
        too_2 = $('#narycardto-2').val()

        if (too_1 == "") && (too_2 == "")
          too_1 = too_1.concat "0"
          too_aux = too_1.concat ".."
          too_2 = too_2.concat "*"
          @too = too_aux.concat too_2
        else
          if (too_1 == "") && (too_2 != "")
            too_1 = too_1.concat "0"
            too_aux = too_1.concat ".."
            @too = too_aux.concat too_2
          else
            if (too_1 != "") && (too_2 == "")
              too_aux = too_1.concat ".."
              too_2 = too_2.concat "*"
              @too = too_aux.concat too_2
            else
              too_aux = too_1.concat ".."
              @too = too_aux.concat too_2

        name = $("#naryrole-to").val()
        prefix = $("#umlrole_a_editprefix_input").val()

        if name != ""
          if prefix != ""
            name_a = prefix.concat ":"
            @to_role = name_a.concat name
          else
            @to_role = name

    # Edit roles and multiplicity of class involved in this association
    edit_assoc: () ->
      b_name = null
      prefix = null

      this.cardto()
      mult = []
      mult[0] = null
      mult[1] = @too
      roles = []
      roles[0] = null
      roles[1] = @to_role

      @hide()
      gui.uml.iguiuml.set_association_state(@classid, mult, roles, name, false, "binary")

    set_reliconid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

        $("#umlrole_a_editurl_input").attr("disabled", "true")
        $("#umlrole_a_editurl_input").css("background-color", "lightgray")

    get_reliconid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)

exports.views.uml.association.EditAssocView =EditAssocView
