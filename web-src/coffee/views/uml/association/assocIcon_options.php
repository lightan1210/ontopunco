<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   classoptions.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div id="assocIcon_options">

    <input type="hidden" id="voptions_reliconid"
	   name="reliconid" value="<%= reliconid %>" />

     <div class="btn-group btn-group-sm" role="group">
	<button class="btn btn-secondary" type="button"
		title="Edit Association" id="edit_button">
	    Edit Association
	</button>
	<button class="btn btn-secondary" type="button"
		title="Add Class" id="addclass_button">
	    Add Class
	</button>
	<button class="btn btn-danger" type="button"
		title="Delete Association" id="deleteassoc_button">
	    Delete
	</button>
  <button class="btn btn-sm btn-info" type="button" title="i" id="metadata_button">
  Info
  </button>
<!--	<button class="btn btn-danger" type="button"
		title="Close" id="closeassoc_button">
	    Close
	</button> -->
    </div>

</div>
