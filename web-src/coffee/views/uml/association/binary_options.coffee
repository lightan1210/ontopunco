exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this


BinaryOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_binary_options").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click button#uml_binary_button" : "new_relation",
        "click button#uml_binary_class_button" : "new_assoc_class"
        "click button#uml_close_binary_class_button" : "hide"

    cardfrom: () ->
        from_1 = $('#umlcardfrom-1').val()
        from_2 = $('#umlcardfrom-2').val()

        if (from_1 == "") && (from_2 == "")
          from_1 = from_1.concat "0"
          from_aux = from_1.concat ".."
          from_2 = from_2.concat "*"
          @from = from_aux.concat from_2
        else
          if (from_1 == "") && (from_2 != "")
            from_1 = from_1.concat "0"
            from_aux = from_1.concat ".."
            @from = from_aux.concat from_2
          else
            if (from_1 != "") && (from_2 == "")
              from_aux = from_1.concat ".."
              from_2 = from_2.concat "*"
              @from = from_aux.concat from_2
            else
              from_aux = from_1.concat ".."
              @from = from_aux.concat from_2

        @from_role = $('#umlrole-from').val()

    # Retrieve the destination role and multiplicity
    cardto: () ->
        too_1 = $('#umlcardto-1').val()
        too_2 = $('#umlcardto-2').val()

        if (too_1 == "") && (too_2 == "")
          too_1 = too_1.concat "0"
          too_aux = too_1.concat ".."
          too_2 = too_2.concat "*"
          @too = too_aux.concat too_2
        else
          if (too_1 == "") && (too_2 != "")
            too_1 = too_1.concat "0"
            too_aux = too_1.concat ".."
            @too = too_aux.concat too_2
          else
            if (too_1 != "") && (too_2 == "")
              too_aux = too_1.concat ".."
              too_2 = too_2.concat "*"
              @too = too_aux.concat too_2
            else
              too_aux = too_1.concat ".."
              @too = too_aux.concat too_2

        @to_role = $('#umlrole-to').val()

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields.
    #
    # Callback, used when the user clicks on the association button.
    new_relation: () ->
      b_name = null
      prefix = null

      this.cardfrom()
      this.cardto()

      mult = []
      mult[0] = @from
      mult[1] = @too
      roles = []
      roles[0] = @from_role
      roles[1] = @to_role
      name = $("#uml_binary_name").val()
      prefix = $("#uml_a_editprefix_input").val()

      if name != ""
          if prefix != ""
            name_a = prefix.concat ":"
            b_name = name_a.concat name
          else
            b_name = name

      @hide()
      gui.uml.iguiuml.set_association_state @classid, mult, roles, b_name, false, "binary"

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields. *Use an association class in the middle.*
    #
    # Callback, used when the user clicks on the create association class button.
    new_assoc_class: (from, too) ->
      b_name = null
      prefix = null

      this.cardfrom()
      this.cardto()

      mult = []
      mult[0] = @from
      mult[1] = @too
      roles = []
      roles[0] = @from_role
      roles[1] = @to_role
      name = $("#uml_binary_name").val()
      prefix = $("#uml_a_editprefix_input").val()

      if name != ""
          if prefix != ""
            name_a = prefix.concat ":"
            b_name = name_a.concat name
          else
            b_name = name

      @hide()
      gui.gui_instance.set_association_state @classid, mult, roles, b_name, true, "binary"


    # Map the Option value to multiplicity string.
    #
    # @param str {string} The value string.
    # @return {String} A string that represent the multiplicity as in UML.
    map_to_mult : (str) ->
        switch str
            when "zeromany" then "0..*"
            when "onemany" then "1..*"
            when "zeroone" then "0..1"
            when "oneone" then "1..1"

    # Shows current widget and put the name, prefix and url of the uri
    # associated to the binary association
    #
    # @param classid {String} A string representing a class identifier in the current JointJS graph
    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

        $("#uml_a_editurl_input").attr("disabled", "true")
        $("#uml_a_editurl_input").css("background-color", "lightgray")


    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)

exports.views.uml.association.BinaryOptionsView = BinaryOptionsView
