# association_options.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this


AssociationOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_associationoptions").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click button#umlassociation_button" : "new_relation",
        "click button#umlassoc_class_button" : "new_assoc_class"


    cardfrom: () ->
        from_1 = $('#umlcardfrom-1').val()
        from_2 = $('#umlcardfrom-2').val()

        if (from_1 == "") && (from_2 == "")
          from_1 = from_1.concat "0"
          from_aux = from_1.concat ".."
          from_2 = from_2.concat "*"
          @from = from_aux.concat from_2
        else
          if (from_1 == "") && (from_2 != "")
            from_1 = from_1.concat "0"
            from_aux = from_1.concat ".."
            @from = from_aux.concat from_2
          else
            if (from_1 != "") && (from_2 == "")
              from_aux = from_1.concat ".."
              from_2 = from_2.concat "*"
              @from = from_aux.concat from_2
            else
              from_aux = from_1.concat ".."
              @from = from_aux.concat from_2

        @from_role = $('#umlrole-from').val()

    # Retrieve the destination role and multiplicity
    cardto: () ->
        too_1 = $('#umlcardto-1').val()
        too_2 = $('#umlcardto-2').val()

        if (too_1 == "") && (too_2 == "")
          too_1 = too_1.concat "0"
          too_aux = too_1.concat ".."
          too_2 = too_2.concat "*"
          @too = too_aux.concat too_2
        else
          if (too_1 == "") && (too_2 != "")
            too_1 = too_1.concat "0"
            too_aux = too_1.concat ".."
            @too = too_aux.concat too_2
          else
            if (too_1 != "") && (too_2 == "")
              too_aux = too_1.concat ".."
              too_2 = too_2.concat "*"
              @too = too_aux.concat too_2
            else
              too_aux = too_1.concat ".."
              @too = too_aux.concat too_2

        @to_role = $('#umlrole-to').val()

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields.
    #
    # Callback, used when the user clicks on the association button.
    new_relation: () ->
        mult = $("#umlcard").val()
        mult = mult.split ",", 50
        roles = $("#umlroles").val()
        roles = roles.split ",", 50
        name = $("#umlassoc_name").val()
        if name == ""
          name=null
        gui.gui_instance.set_association_state @classid, mult, roles,[name, false]

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields. *Use an association class in the middle.*
    #
    # Callback, used when the user clicks on the create association class button.
    new_assoc_class: (from, too) ->
        mult = $("#umlcard").val()
        mult = mult.split ",", 50
        roles = $("#umlroles").val()
        roles = roles.split ",", 50
        name = $("#umlassoc_name").val()
        if name == ""
          name=null
        @hide()
        gui.gui_instance.set_association_state(@classid, mult, roles, [name, true])


    # Map the Option value to multiplicity string.
    #
    # @param str {string} The value string.
    # @return {String} A string that represent the multiplicity as in UML.
    map_to_mult : (str) ->
        switch str
            when "zeromany" then "0..*"
            when "onemany" then "1..*"
            when "zeroone" then "0..1"
            when "oneone" then "1..1"


    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.uml.association.AssociationOptionsView = AssociationOptionsView
