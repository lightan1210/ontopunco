# nary_options.coffee --
# Copyright (C) 2018 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this


NaryOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_nary_options").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click button#nary_button" : "new_relation",
        "click button#nary_class_button" : "new_assoc_class"
        "click button#uml_close_nary_button" : "hide"

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields.
    #
    # Callback, used when the user clicks on the association button.
    new_relation: () ->
      n_name = null
      prefix = null
      mult = []
      mult[0] = ""
      mult[1] = ""
      roles = []
      roles[0] = ""
      roles[1] = ""

      name = $("#uml_nary_name").val()
      prefix = $("#uml_nary_name_prefix").val()

      if name != ""
          if prefix != ""
            name_a = prefix.concat ":"
            n_name = name_a.concat name
          else
            n_name = name

      @hide()
      gui.uml.iguiuml.set_association_state @classid, mult, roles, n_name, false, "nary"

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields. *Use an association class in the middle.*
    #
    # Callback, used when the user clicks on the create association class button.
    new_assoc_class: (from, too) ->
      n_name = null
      prefix = null
      mult = []
      mult[0] = ""
      mult[1] = ""
      roles = []
      roles[0] = ""
      roles[1] = ""
      name = $("#uml_nary_name").val()
      prefix = $("#uml_nary_name_prefix").val()

      if name != ""
          if prefix != ""
            name_a = prefix.concat ":"
            n_name = name_a.concat name
          else
            n_name = name

      @hide()
      gui.uml.iguiuml.set_association_state(@classid, mult, roles, n_name, true, "nary")


    # Map the Option value to multiplicity string.
    #
    # @param str {string} The value string.
    # @return {String} A string that represent the multiplicity as in UML.
    map_to_mult : (str) ->
        switch str
            when "zeromany" then "0..*"
            when "onemany" then "1..*"
            when "zeroone" then "0..1"
            when "oneone" then "1..1"


    set_classid: (@classid) ->
      viewpos = graph.getCell(@classid).findView(paper).getBBox()

      this.$el.css(
          top: viewpos.y + 50,
          left: viewpos.x + 100,
          position: 'absolute',
          'z-index': 1
          )
      this.$el.show()

      $("#uml_nary_name_url").attr("disabled", "true")
      $("#uml_nary_name_url").css("background-color", "lightgray")

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.uml.association.NaryOptionsView = NaryOptionsView
