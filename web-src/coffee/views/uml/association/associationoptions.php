<?php
/*

Copyright 2018, Grupo de Investigación en Lenguajes e Inteligencia Artificial (GILIA)

Author: Facultad de Informática, Universidad Nacional del Comahue

isaoptions.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


?>


<div class="relationOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden" id="umlrelationoptions_classid"  name="classid"  value="<%= classid %>" />
    <div data-role="controlgroup" data-mini="true" data-type="horizontal" style="float: left">
      <form id="name-rel">
         <input data-mini="true" placeholder="Name" type="text" size="4" maxlength="10" id="umlassoc_name" />
         <div data-role="controlgroup" data-mini="true" data-type="horizontal">
             <a class="ui-btn ui-corner-all ui-icon-arrow-r ui-btn-icon-notext" type="button" id="umlassociation_button">Association</a>
             <a class="ui-btn ui-corner-all ui-btn-icon-notext" type="button" id="umlassoc_class_button">Association Class</a>
         </div>
      </form>
    </div>

    <input type="hidden" id="umlrelationoptions_classid"  name="classid"  value="<%= classid %>" />
    <div data-role="controlgroup" data-mini="true" data-type="horizontal" style="float: left">
      <form id="right-rel">
         <input type="hidden" id="umlrelationoptions_classid" name="classid" value="<%= classid %>" />
         <input data-mini="true" placeholder="Cardinalities: 0..1,0..*,..." type="text" id="umlcard" size="18" />
         <input data-mini="true" placeholder="Roles: role1,role2,role3,..." type="text" id="umlroles" size="18" />
      </form>
    </div>
    
</div>
