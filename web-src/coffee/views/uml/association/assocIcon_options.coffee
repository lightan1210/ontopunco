exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this

AssocIconOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_assocIcon_options").html() )
        this.$el.html(template({reliconid: @reliconid}))

    events:
        "click button#deleteassoc_button" : "delete_association",
        "click button#edit_button" : "edit_association",
        "click button#addclass_button" : "addclass_association",
        "click button#metadata_button" : "ns",
        "click button#closeassoc_button" : "hide"

    delete_association: () ->
      gui.uml.iguiuml.widgets.hide_options()
      gui.uml.iumladapter.delete_class @reliconid

    # Edit roles and multiplicity of class involved in this association
    edit_association: () ->
      gui.uml.iguiuml.widgets.hide_options()
      gui.uml.iumlwidgets.set_editassociation_classid @reliconid, "edit"
      gui.state_inst.selectionstate_inst.reset_click()

    # Add class to this association
    addclass_association: () ->
      mult = [null,""]
      roles = [null,""]
      gui.uml.iguiuml.widgets.hide_options()
      gui.uml.iguiuml.set_association_state @reliconid, mult, roles, name, false, "n-ary"
      gui.state_inst.selectionstate_inst.reset_click()

    set_reliconid: (@reliconid) ->
        viewpos = graph.getCell(@reliconid).findView(paper).getBBox()

        this.$el.css
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
        this.$el.show()

    ns: (event) ->
      gui.uml.iumlwidgets.hide_options()
      info = gui.gui_instance.current_gui.diagadapter.show_assoc_info @reliconid
      gui.uml.iumlwidgets.show_info info
      gui.state_inst.selectionstate_inst.reset_click()

    get_reliconid: () ->
        return @reliconid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)

exports.views.uml.association.AssocIconOptionsView = AssocIconOptionsView
