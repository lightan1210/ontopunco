# error_widget.coffee --
# Copyright (C) 2018 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this


InfoWidgetView = Backbone.View.extend(
    initialize: () ->
        this.render()
        # We need to initialize the error-popup div because
        # jquery-mobile.js script is loaded before this script.
        #this.$el.hide()
    render: () ->
        template = _.template $("#template_infowidget").html()
        this.$el.html template({})

    show: (message) ->
      this.$el.find("#info_widget").modal('show')
      $("#infotex").empty()
      console.log(message)

      $("#infotex").append('<b>Class</b></br>')
      message[0].forEach (elem, index, arr) ->
        $("#infotex").append(elem)
        $("#infotex").append('</br>')

      $("#infotex").append('</br><b>DataProperties</b></br>')
      message[1].forEach (elem, index, arr) ->
        elem.forEach (field, ind, arry) ->
          $("#infotex").append(field)
          $("#infotex").append('</br>')
        $("#infotex").append('</br>')

    events:
        "click a#infowidget_hide_btn" : "hide"

    hide: () ->
        $("#info_widget").modal('toggle')

    enable: () ->
        this.$el.show()

    # Hide and disable the widget.
    disable: () ->
        this.$el.hide()
)

exports.views.uml.InfoWidgetView = InfoWidgetView
