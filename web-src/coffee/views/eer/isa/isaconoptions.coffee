
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.eer = exports.views.eer ? this
exports.views.eer.isa = exports.views.eer.isa ? this


IsaIconOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_isaiconoptions").html() )
        this.$el.html(template({isaiconid: @isaiconid}))

    events:
        "click a#deleteisa_button" : "delete_isa",
        "click a#addchild_button" : "edit_isa",

    delete_isa: () ->
          console.log('REMOVER ISA')
          gui.gui_instance.hide_options()
          gui.gui_instance.delete_class(@isaiconid)

    # Add child to isa
    edit_isa: () ->
          console.log('AGREGAR HIJO ISA')
          gui.gui_instance.hide_options()
          guiinst.set_isa_state(@isaiconid)

    set_isaiconid: (@isaiconid) ->
        viewpos = graph.getCell(@isaiconid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_isaiconid: () ->
        return @isaiconid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.eer.IsaIconOptionsView = IsaIconOptionsView
