<?php
$GLOBALS['config'] = [];

/**
   What kind of environment is this?
   * 'devel' : Development. Non-minimal CSS and JS libraries and PHP
   code with information return.
   * 'prod' : Production. Minimal CSS and JS libraries and PHP
   without development output.
 */
$GLOBALS['environment']='devel';

/**
   Where I can store temporary files?

   For executing Racer, I need to store the OWLlink file, where I
   should do this?

   Remember: Apache (represented as httpd, apache or www-data user in
   some systems) should have write perms there.
 */


$GLOBALS['config']['temporal_path'] = '/srv/http/ontopunco/run/';
$GLOBALS['config']['sparqldl_api_path'] = '/srv/http/ontopunco/run/sparql-dl-api/target/';
/**
   Where is the reasoner?

   By default we provide a Racer and a konclude program inside the temporal_path
   (at wicom/run/), but if you want to use another program you
   have to set this value with the path.
 */
$GLOBALS['config']['racer_path'] = $GLOBALS['config']['temporal_path'];
$GLOBALS['config']['konclude_path'] = $GLOBALS['config']['temporal_path'];
$GLOBALS['config']['automap_path'] = $GLOBALS['config']['temporal_path']."automap/";

$GLOBALS['config']['ontop_path'] = $GLOBALS['config']['temporal_path']."ontop/";

$GLOBALS['config']['sparqldl_path'] = $GLOBALS['config']['sparqldl_api_path'];
$GLOBALS['config']['widoco_path'] = $GLOBALS['config']['temporal_path'];


//function setDBParameters($dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema){

  
//};

/**
   @name Database Configuration.
*/
//@{
/**
   Database host.

   For specify the port use "HOST:PORT".

   Example: `localhost:3000`

   @see http://php.net/manual/en/mysqli.construct.php
*/
$GLOBALS['config']['db']['host'];

/**
   Database user name.
 */

$GLOBALS['config']['db']['user'];

/**
   Database password
 */
$GLOBALS['config']['db']['password'];

/**
   Database name.
 */
$GLOBALS['config']['db']['database'];

/**
   Driver name.
 */
$GLOBALS['config']['db']['driver'];

/**
   Schema name.
 */
$GLOBALS['config']['db']['schema'];

$GLOBALS['config']['db']['charset'] = 'utf8';
//@}

?>
