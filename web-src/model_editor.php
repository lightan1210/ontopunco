<?php
require_once('template.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
	<?php
	include '_headincludes.php';
	?>
	<title>crowd, the ontologist</title>

        <link rel="stylesheet" href="./css/interfaz.css" />
    </head>

    <body>
	<header>
	    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand crowd-header text-white">c r o w d</a>
		<button class="navbar-toggler" type="button"
			data-toggle="collapse" data-target="#navbarNav"
			aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav mr-auto">
			<li class="nav-item">
			    <a href="#details" class="nav-link">Tools</a>
			</li>
			<li class="nav-item">
			    <a href="#metamodel_widget" data-toggle="modal"
			       class="nav-link">Switch Modelling Language</a>

			</li>
			<li class="nav-item">
			    <a href="#OBDA_widget" data-toggle="modal"
			       class="nav-link">OBDA</a>

			</li>
			<!--<div id="eer_toolbar_placeholder"></div>-->
			<div id="lang_tools_placeholder"></div>
		    </ul>
<!--		    <form class="form-inline mt-2 mt-md-0">
			<div id="trafficlight"></div>
    </form> -->
		</div>
	    </nav>
	</header>

	<!-- ---------------------------------------------------------------------- -->

 <main role="main">

	    <div class="container-fluid">

        <div id="container"></div>

		<a id="details"/>

		<div class="btn-group" role="group" aria-label="details">
		    <button type="button" class="btn btn-secondary"
			    data-toggle="modal" data-target="#reasoning_widget">
			Reasoning
		    </button>
<!--		    <button type="button" class="btn btn-secondary"
			    data-toggle="modal" data-target="#translation_widget">
			Translation
    </button> -->
		    <button type="button" class="btn btn-secondary"
			    data-toggle="modal" data-target="#insertowllink_widget">
			Insert OWLlink
		    </button>

		</div>

<!--		<div class="btn-group" role="group" aria-label="verbalization">
		    <button type="button" class="btn btn-secondary"
			    data-toggle="collapse"
			    data-target="#verbalisation-page"
			    aria-expanded="false"
			    aria-controls="verbalisation-page">
			Verbalisation
		    </button>
		</div> -->


		<div class="btn-group" role="group" aria-label="user">
		    <button type="button" class="btn btn-secondary"
			    data-target="#loginWidget" data-toggle="modal">
			Login / Logout
		    </button>
		    <button type="button" class="btn btn-secondary"
			    data-target="#saveloadjsonwidget" data-toggle="modal">
			Save / Load
		    </button>
		</div>

		<div class="btn-group" role="group" aria-label="Import/Export">
		    <button type="button" class="btn btn-secondary"
			    data-toggle="modal" data-target="#importjson_widget">
			Import JSON
		    </button>
		    <button type="button" class="btn btn-secondary"
			    data-toggle="modal" data-target="#exportjson_widget">
			Export JSON
		    </button>
		</div>


	    <button type="button" class="btn btn-secondary"
			  data-toggle="modal" data-target="#clear_widget">
		Clear
	    </button>

	    <button type="button" class="btn btn-secondary"
		    data-toggle="modal" data-target="#namespaces_widget">
		Namespaces
	    </button>

	    <button type="button" class="btn btn-secondary"
		    data-toggle="modal" data-target="#importowl_widget">
		Import OWL
	    </button>

    </div>

		<!-- ---------------------------------------------------------------------- -->
		<!-- Details page -->

		<!-- ---------------------------------------------------------------------- -->
		<!-- Verbalisation page -->

		<div class="collapse" id="verbalisation-page">

		    <div id="verbalisation_panel">
			<h1 id="verbalisation">Verbalisation</h1>
			<div id="verbalisation_details">
			    <h3 class="ui-bar ui-bar-a ui-corner-all">Verbalisation Output</h3>
			    <div class="ui-body">
				<div id="html-output"></div>
				<textarea cols="10" id="source_verbalisation">Verbalisation Here!</textarea>
		    		<a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all" type="button" id="verbalise_button"
				   onclick="guiinst.to_fol()">Verbalise Again</a>
			    </div>
			</div>
		    </div>
		</div> <!-- collapse -->

		<?php
		include 'all_templates.php';
		include 'placeholders.php';
		?>


	    </div><!-- container -->


	</main>

	<footer class="text-muted">
	    <div class="container">
		<p class="float-right">
		    <a href="#">Back to top</a>
		</p>
		<a href="http://faiweb.uncoma.edu.ar">
		    Facultad de Informática,
		    Universidad Nacional del Comahue (Argentina)
		</a>
	    </div>
	</footer>


        <?php include '_footincludes.php' ?>
	<script src="./js/csstheme.js"></script>
	<script src="./js/backbone_views.js"></script>
	<script src="./js/model.js"></script>
	<?php if ($_GET['type'] == 'UML'){ ?>
	    <script src="./js/interface-uml.js"></script>
        <?php } elseif ($_GET['type'] == 'EER') { ?>
		<script src="./js/interface-eer.js"></script>
	<?php } elseif ($_GET['type'] == 'ORM') { ?>
		    <script src="./js/interface-orm.js"></script>
	<?php } else {
	    die('No interface selected');
	} ?>
	<script src="./js/login.js"></script>
	<script src="./js/gui.js"></script>

	<?php if (array_key_exists('prueba', $_GET) && $_GET['prueba'] == 1){ ?>
	    <script src="./js/prueba.js"></script>
	<?php } ?>

    </body>
</html>
