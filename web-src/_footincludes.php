<?php
include('./config/config.php');

if ($environment == 'production'){ ?>
    <script src="./js/libs/jquery-3.2.1.min.js"></script>    
    <script src="./js/libs/popper.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>

    <script src="./js/libs/lodash.min.js"></script>
    
    <script src="./js/libs/backbone-min.js"></script>
    
    <script src="./js/libs/joint.min.js"></script>
    <script src="./js/libs/joint.shapes.erd.min.js"></script>
    <script src="./js/libs/joint.shapes.uml.min.js"></script>
    <script src="./js/libs/joint.shapes.orm.min.js"></script>

<?php }else{ ?>    
    <script src="./js/libs/jquery-3.2.1.js"></script>    
    <script src="./js/libs/popper.js"></script>
    <script src="./js/libs/bootstrap.js"></script>

    <script src="./js/libs/lodash.min.js"></script>
    <script src="./js/libs/backbone.js"></script>
    <script src="./js/libs/joint.js"></script>
    <script src="./js/libs/joint.shapes.erd.js"></script>
    <script src="./js/libs/joint.shapes.uml.js"></script>
    <script src="./js/libs/joint.shapes.orm.js"></script>
<?php } ?>

<script src="./js/model.js"></script>
