<?php
/*

   Copyright 2018 Giménez, Christian

   Author: Giménez, Christian

   all_templates.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


insert_template("loginwidget", "common");
insert_template("saveloadjsonwidget", "common");

insert_template("errorwidget","common");
insert_template("trafficlight","common");
insert_template("insertowllink","common");
insert_template("importjson","common");
insert_template("exportjson","common");
insert_template("done_widget","common");
insert_template("clear", "common");
insert_template("metamodel_widget", "common");
insert_template("translation", "common");
insert_template("reasoning", "common");
insert_template('namespaces', 'common');
insert_template("OBDA_widget", "common");

insert_template("tools_navbar","uml/classes");
insert_template("classoptions","uml/classes");
insert_template("editclass", "uml/classes");
insert_template("edit_attributes", "uml/classes");
insert_template("associationoptions","uml/association");
insert_template("assocIcon_options", "uml/association");
insert_template("generalisationoptions","uml/generalisation");
insert_template("generalisationiconoptions", "uml/generalisation");
insert_template("association_type", "uml/association");
insert_template("nary_options", "uml/association");
insert_template("binary_options", "uml/association");
insert_template("editassoc", "uml/association");

insert_template("reasoning_widget", "uml");
insert_template("importowl","uml");
insert_template("tools_uml","uml");
insert_template("translate_widget", "uml");
insert_template("infowidget", "uml");

// insert_template("tools_navbar_erd","eer/entity");
insert_template("editentity","eer/entity");
insert_template("entityoptions","eer/entity");
insert_template("relationoptions","eer/relationship");
insert_template("isaoptions","eer/isa");
insert_template("attroptions","eer/attributes");

insert_template("tools_eer","eer");
?>
