<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian


   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\OBDA;

load("mapper.php");
load("config.php", "../../config/");


use Wicom\OBDA\Mapper;

class Automap extends Mapper{


    //TODO: Change PROGRAM_CMD and FILES_PATH into configuration variables.

   
    const PROGRAM_CMD = "AutoMap.jar";
   
   
    function run($dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema, $temporalID){
    	
      $PROGRAM_PARAMS = "-db jdbc:postgresql://". $dbIp."/".$dbName. " -schema ". $dbSchema." -driver ".$dbDriver." -u ".$dbUsername." -p ".$dbPassword." -n ontologia_putativa";
        

        $temporal_path = $GLOBALS['config']['temporal_path'];
        $automap_path = $GLOBALS['config']['automap_path'];
        $file_path = $temporal_path . "crowd_".$temporalID.".owl";
        $out_file_path = $temporal_path .$temporalID;

        $automap_path .= Automap::PROGRAM_CMD;
        $commandline = "java -jar " . $automap_path . " " .$PROGRAM_PARAMS." -d ".$file_path." -o ".$out_file_path;


        $this->check_files($temporal_path, $automap_path, $file_path);
        exec($commandline,$answer);
     
        $mappings_out_file = fopen($out_file_path . "_automap4obda.r2rml", "r");
        $mappings_answer = fread($mappings_out_file, filesize($out_file_path . "_automap4obda.r2rml"));
        fclose($mappings_out_file);

        array_push($this->col_answers, $mappings_answer);
    }

    /**
       Check for program and input file existance and proper permissions.

       @return true always
       @exception Exception with proper message if any problem is founded.
    */
    function check_files($temporal_path, $automap_path, $file_path){
        if (! is_dir($temporal_path)){
            throw new \Exception("Temporal path desn't exists!
Are you sure about this path?
temporal_path = \"$temporal_path\"");
        }

        if (!file_exists($file_path)){
            throw new \Exception("Temporal file doesn't exists, please create one at '$file_path'.");
        }

        if (!is_readable($file_path)){
            throw new \Exception("Temporal file cannot be read.
Please set the write and read permissions for '$file_path'");
        }

        if (file_exists($file_path) and !is_writable($file_path)){
            throw new \Exception("Temporal file is not writable, please change the permissions.
Check the permissions on '${file_path}'.");
        }

        if (!file_exists($automap_path)){
            throw new \Exception("The Automap4OBDA program has not been found...
You told me that '$racer_path' is the Automap4OBDA program, is this right? check your 'web-src/config/config.php' configuration file.");
        }

        if (!is_executable($automap_path)){
            throw new \Exception("The Automap4OBDA program is not executable...
Is the path '$automap_path' right? Are the permissions setted properly?");
        }

        return true;
    }
}

?>
