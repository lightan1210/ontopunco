<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian


   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//namespace Wicom\Ontop;
namespace Wicom\OBDA;

load("obdaframework.php");
load("config.php", "../../config/");

//use Wicom\Ontop\Connector;

use Wicom\OBDA\OBDAFramework;

class Ontop extends OBDAFramework{


    //TODO: Change PROGRAM_CMD and FILES_PATH into configuration variables.

    /**
       The Ontop command to execute with all its parameters.
     */
    const PROGRAM_CMD = "ontop";

    function run($dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema, $temporalID){

     $PROGRAMS_PARAMS = "-m ". $GLOBALS['config']['temporal_path'].$temporalID."_automap4obda.r2rml". " -t " . $GLOBALS['config']['temporal_path']."crowd_".$temporalID.".owl". " -o ". $GLOBALS['config']['temporal_path']."qAnswer_".$temporalID.".csv" . " -l jdbc:postgresql://". $dbIp. "/".$dbName. " -u ". $dbUsername. " -p ". $dbPassword . " -d ". $dbDriver . " -q ". $GLOBALS['config']['temporal_path']."query_".$temporalID.".txt";

        $temporal_path = $GLOBALS['config']['temporal_path'];
        $ontop_path = $GLOBALS['config']['ontop_path'];



        $file_path = $temporal_path . "crowd_".$temporalID.".owl";
        $out_file_path = $temporal_path . "qAnswer_".$temporalID.".csv";
        $ontop_path .= Ontop::PROGRAM_CMD;
        $commandline = $ontop_path. " query ".$PROGRAMS_PARAMS;


        $this->check_files($temporal_path, $ontop_path, $file_path);

        exec($commandline,$answer);

        $out_file = fopen($out_file_path , "r");
        $answer = fread($out_file, filesize($out_file_path));
        fclose($out_file);

        array_push($this->col_answers, $answer);
    }

    /**
       Check for program and input file existance and proper permissions.

       @return true always
       @exception Exception with proper message if any problem is founded.
    */
    function check_files($temporal_path, $ontop_path, $file_path){
        if (! is_dir($temporal_path)){
            throw new \Exception("Temporal path desn't exists!
Are you sure about this path?
temporal_path = \"$temporal_path\"");
        }

        if (!file_exists($file_path)){
            throw new \Exception("Temporal file doesn't exists, please create one at '$file_path'.");
        }

        if (!is_readable($file_path)){
            throw new \Exception("Temporal file cannot be read.
Please set the write and read permissions for '$file_path'");
        }

        if (file_exists($file_path) and !is_writable($file_path)){
            throw new \Exception("Temporal file is not writable, please change the permissions.
Check the permissions on '${file_path}'.");
        }

        if (!file_exists($ontop_path)){
            throw new \Exception("The Ontop program has not been found...
You told me that '$ontop_path' is the Ontop program, is this right? check your 'web-src/config/config.php' configuration file.");
        }

          if (!is_executable($ontop_path)){
            throw new \Exception("The Ontop program is not executable...
Is the path '$ontop_path' right? Are the permissions setted properly?");
        }

        return true;
    }
}

?>
