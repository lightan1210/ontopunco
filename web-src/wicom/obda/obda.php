<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   translator.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\OBDA;

//load("config.php", "../../config/");
load("translator.php", "../translator/");
load("automapmapper.php");
load("ontopframework.php");
load("crowd_uml.php", "../translator/strategies/");
load("owlbuilder.php", "../translator/builders/");

use Wicom\Translator\Translator;
use Wicom\Translator\Strategies\UMLcrowd;
use Wicom\Translator\Builders\OWLBuilder;
use Wicom\OBDA\Automap;

use Wicom\OBDA\Ontop;

use function \load;

use function \json_decode;


class OBDAInitializer {
    
    var $obdaFramework;
    var $mappingTool;
    var $queries;
    var $ontology;
    var $temporalID;
    var $dbIp;
    var $dbUsername;
    var $dbPassword;
    var $dbDriver;
    var $dbName;
    var $dbSchema;


    function __construct($mapTool, $obdaFrw, $json_str, $mappings, $query, $dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema, $temporalID){

        $this->obdaFramework = $obdaFrw;
        $this->mappingTool = $mapTool;
        //$this->queries = $query;
        
        $this->dbIp = $dbIp;
        $this->dbUsername = $dbUsername;
        $this->dbPassword = $dbPassword;
        $this->dbDriver = $dbDriver;
        $this->dbName = $dbName;
        $this->dbSchema = $dbSchema;
        $this->temporalID = $temporalID;

        $ontology = $this->jsonToOWL($json_str);



	    //$this->setParameters($dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema);

      /*if($mapTool == null){
        $this->createMappingsFile($mappings);
      }*/

	    $this->createQueryFile($query);

      $this->createOWLFile($ontology);

    }


    function jsonToOWL($json){
        $trans = new Translator(new UMLcrowd(), new OWLBuilder());

        $ontology = $trans->to_owl2($json);

        return $ontology;
    }

    function createQueryFile($queries){

    	$queries_file = fopen($GLOBALS['config']['temporal_path']."query_".$this->temporalID.".txt", "w");
        fwrite($queries_file, $queries);
        fclose($queries_file);

    }



    function createOWLFile($ontology){

      $owl_file = fopen($GLOBALS['config']['temporal_path']."crowd_".$this->temporalID.".owl", "w");
        fwrite($owl_file, $ontology);
        fclose($owl_file);

    }


      function createMappingsFile($mappings){

      $mappings_file = fopen($GLOBALS['config']['temporal_path']."mappingss_automap4obda.r2rml", "w");
        fwrite($mappings_file, $mappings);
        fclose($mappings_file);

    }


    function setParameters($dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema){



    	$GLOBALS['config']['db']['host'] = $dbIp;

	     /**
	        Database user name.
	      */

	    $GLOBALS['config']['db']['user'] = $dbUsername;

	     /**
	        Database password
	      */
	    $GLOBALS['config']['db']['password'] = $dbPassword;

	     /**
	        Database name.
	      */
	    $GLOBALS['config']['db']['database'] = $dbName;

	     /**
	        Driver name.
	      */
	    $GLOBALS['config']['db']['driver'] = $dbDriver;

	     /**
	        Schema name.
	      */
	    $GLOBALS['config']['db']['schema'] = $dbSchema;

    }

    
       

    function deleteFiles(){
    	unlink($GLOBALS['config']['temporal_path']."query_".$this->temporalID.".txt");
      unlink($GLOBALS['config']['temporal_path']."qAnswer_".$this->temporalID.".csv");
      unlink($GLOBALS['config']['temporal_path'].$this->temporalID."_automap4obda.r2rml");
      unlink($GLOBALS['config']['temporal_path'].$this->temporalID."_automap4obda.owl");
      unlink($GLOBALS['config']['temporal_path'].$this->temporalID."_ol_automap4obda.owl");
      unlink($GLOBALS['config']['temporal_path'].$this->temporalID."_ol_automap4obda.text");
      unlink($GLOBALS['config']['temporal_path']."crowd_".$this->temporalID.".owl");
    }


    function getMappings(){

    	$this->mappingTool-> run($this->ontology);


    	$mappings = $this->mappingTool->get_col_answers()[0];


    	return $mappings;

    }


    function obda(){

      $mappings = null;

      if($this->mappingTool != null){
          $this->mappingTool-> run($this->dbIp, $this->dbUsername, $this->dbPassword, $this->dbDriver, $this->dbName, $this->dbSchema, $this->temporalID);

          $mappings = $this->mappingTool->get_col_answers()[0];
      }

    	$this->obdaFramework->run($this->dbIp, $this->dbUsername, $this->dbPassword, $this->dbDriver, $this->dbName, $this->dbSchema, $this->temporalID);

    	$query_answer = $this->obdaFramework->get_col_answers()[0];

    	$this->deleteFiles();

      $jsonData = array(
          'mappings' => $mappings,
          'query_answer' => $query_answer
      );

      $jsonstring = json_encode($jsonData);

    	return $jsonstring;
    
    }


    /*function allTogether(){

    	$this->mappingTool-> run($this->ontology);

    	$this->obdaFramework->run($this->ontology);

      $mappings = $this->mappingTool->->get_col_answers()[0];

    	$query_answer = $this->obdaFramework->get_col_answers()[0];

    	

    	return $query_answer;
    }*/
   

}
