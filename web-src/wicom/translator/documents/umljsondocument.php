<?php
/*

Copyright 2018

Author: GILIA

document.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Wicom\Translator\Documents;

use function \load;
load('jsondocument.php');

/**
Json structure for UML digrams to be imported in crowd

{"classes":[
	{"name":"Person","attrs":[{"name":"dni","datatype":"String"},
														{"name":"firstname","datatype":"String"},
														{"name":"surname","datatype":"String"},
														{"name":"birthdate","datatype":"Date"}],
									 "methods":[],
									 "position":{"x":20,"y":20}},
	{"name":"Student","attrs":[{"name":"id","datatype":"String"},
														 {"name":"enrolldate","datatype":"Date"}],
										"methods":[],
										"position":{"x":491,"y":107}},
  {"name":"Class1","attrs":[],
									 "methods":[],
									 "position":{"x":1025,"y":104}}],
"links":[
	{"name":"r1","classes":["Student"],
							 "multiplicity":null,
							 "roles":[null,null],
							 "type":"generalization",
							 "parent":"Person",
							 "constraint":[]},
	{"name":"R1","classes":["Student","Class1"],
							 "multiplicity":["2..4","1..*"],
							 "roles":["e","c"],
							 "type":"association"}]
}

array(2) {
  ["classes"]=>
  array(3) {
    [0]=> array(4) {["name"]=>string(6) "Person"
      ["attrs"]=>
      array(4) {
        [0]=>
        array(2) {
          ["name"]=>
          string(3) "dni"
          ["datatype"]=>
          string(6) "String"
        }
        [1]=>
        array(2) {
          ["name"]=>
          string(9) "firstname"
          ["datatype"]=>
          string(6) "String"
        }
        [2]=>
        array(2) {
          ["name"]=>
          string(7) "surname"
          ["datatype"]=>
          string(6) "String"
        }
        [3]=>
        array(2) {
          ["name"]=>
          string(9) "birthdate"
          ["datatype"]=>
          string(4) "Date"
        }
      }
      ["methods"]=>
      array(0) {
      }
      ["position"]=>
      array(2) {
        ["x"]=>
        int(20)
        ["y"]=>
        int(20)
      }
    }
    [1]=>
    array(4) {
      ["name"]=>
      string(7) "Student"
      ["attrs"]=>
      array(2) {
        [0]=>
        array(2) {
          ["name"]=>
          string(2) "id"
          ["datatype"]=>
          string(6) "String"
        }
        [1]=>
        array(2) {
          ["name"]=>
          string(10) "enrolldate"
          ["datatype"]=>
          string(4) "Date"
        }
      }
      ["methods"]=>
      array(0) {
      }
      ["position"]=>
      array(2) {
        ["x"]=>
        int(491)
        ["y"]=>
        int(107)
      }
    }
    [2]=>
    array(4) {
      ["name"]=>
      string(6) "Class1"
      ["attrs"]=>
      array(0) {
      }
      ["methods"]=>
      array(0) {
      }
      ["position"]=>
      array(2) {
        ["x"]=>
        int(1025)
        ["y"]=>
        int(104)
      }
    }
  }
  ["links"]=>
  array(2) {
    [0]=>
    array(7) {
      ["name"]=>
      string(2) "r1"
      ["classes"]=>
      array(1) {
        [0]=>
        string(7) "Student"
      }
      ["multiplicity"]=>
      NULL
      ["roles"]=>
      array(2) {
        [0]=>
        NULL
        [1]=>
        NULL
      }
      ["type"]=>
      string(14) "generalization"
      ["parent"]=>
      string(6) "Person"
      ["constraint"]=>
      array(0) {
      }
    }
    [1]=>
    array(5) {
      ["name"]=>
      string(2) "R1"
      ["classes"]=>
      array(2) {
        [0]=>
        string(7) "Student"
        [1]=>
        string(6) "Class1"
      }
      ["multiplicity"]=>
      array(2) {
        [0]=>
        string(4) "2..4"
        [1]=>
        string(4) "1..*"
      }
      ["roles"]=>
      array(2) {
        [0]=>
        string(1) "e"
        [1]=>
        string(1) "c"
      }
      ["type"]=>
      string(11) "association"
    }
  }
}

*/

class UMLJSONDocument extends JSONDocument{

	protected $classes = [];
	protected $links = [];
	protected $content = NULL;

	function __construct(){
		$ontologyIRI = [];
		$defaultIRIs = [];
		$IRIs = [];
		$classes = [];
		$links = [];
		$this->content = ["namespaces" => [
													"ontologyIRI" => $ontologyIRI,
													"defaultIRIs" => $defaultIRIs,
													"IRIs" => $IRIs
												],
											"classes" => $classes,
											"links" => $links
										];
	}

	function to_json(){
		return json_encode($this->content);
	}

	function set_prefixes($prefixes){
		foreach ($prefixes as $p){
			array_push($this->content["namespaces"]["defaultIRIs"], $p);
		}
	}

	function set_ontologyIRI($ontologyIRI){
		array_push($this->content["namespaces"]["ontologyIRI"], $ontologyIRI);
	}

	function get_ontologyIRI(){
		return $this->content["namespaces"]["ontologyIRI"][0]["value"];
	}

	/**
	@param $classname {string}
	@param $attrs {array} [[$attname, $datatype],...,[$attnameN, $datatypeN]]

	["classes"]=>
  array(1) {
    [0]=> array(3) {["name"]=>string(6) "Person"
      ["attrs"]=>
      array(4) {
        [0]=>array(2) {["name"]=> string(3) "dni" ["datatype"]=>string(6) "String"}
        [1]=>array(2) {["name"]=>string(9) "firstname"["datatype"]=>string(6) "String"}
        [2]=>array(2) {["name"]=>string(7) "surname"["datatype"]=>string(6) "String"}
        [3]=>array(2) {["name"]=>string(9) "birthdate"["datatype"]=>string(4) "Date"}
      }
      ["methods"]=>array(0) {}
    }
	**/
	function insert_class_without_attr($classname){
		$class_array = ["name" => $classname, "attrs" => [], "methods" => []];
		array_push($this->content["classes"],$class_array);
	}


	function insert_attribute($attr, $class, $datatype){
		$attr_array = ["name" => null, "datatype" => null];

		$i = 0;
		$classes = count($this->content["classes"]);

		while ($i < $classes){

			if (strcmp($this->content["classes"][$i]["name"], $class) == 0){
				array_push($this->content["classes"][$i]["attrs"], ["name" => $attr, "datatype" => $datatype]);
			}
			$i++;
		}
	}

	/**
	["classes"]=>
  array(2) {
    [0]=> array(3) {["name"]=>string(6) "Person"
      ["attrs"]=>array(0) {}
      ["methods"]=>array(0) {}
    }
		[1]=> array(3) {["name"]=>string(6) "Student"
      ["attrs"]=>array(0) {}
      ["methods"]=>array(0) {}
    }
	}
		["links"]=>
	  array(1) {
	    [0]=>array(7) {["name"]=> string(2) "r1"
										 ["classes"]=>array(1) {[0]=>string(7) "Student"}
										 ["multiplicity"]=>NULL
										 ["roles"]=>array(2) {[0]=>NULL[1]=>NULL}
	      			       ["type"]=>string(14) "generalization"
	      						 ["parent"]=>string(6) "Person"
										 ["constraint"]=>array(0) {}
	    }
	}

	@param $classes {array} ["name1", "name2",...,"nameN"]
	@param $parent {string} "parent1"
	@param $constraints ["disjont","covering"]
	**/
	function insert_subsumption($classes, $parent, $constraints = []){
		$array_sub = [];
		$ontologyIRI = $this->get_ontologyIRI();
		$array_sub = ["name" => $ontologyIRI."#s1",
									"classes" => $classes,
									"multiplicity" => NULL,
									"roles" => [NULL,NULL],
									"type" => "generalization",
									"parent" => $parent,
									"constraint" => $constraints];
		array_push($this->content["links"],$array_sub);
	}

	/**
	["classes"]=>
	array(2) {
		[0]=> array(3) {["name"]=>string(6) "Person"
			["attrs"]=>array(0) {}
			["methods"]=>array(0) {}
		}
		[1]=> array(3) {["name"]=>string(6) "Student"
			["attrs"]=>array(0) {}
			["methods"]=>array(0) {}
		}
	}
		["links"]=>
		array(1) {
				[0]=>array(5) {["name"]=>string(2) "R1"
				               ["classes"]=>array(2) {[0]=>string(7) "Student"
											                        [1]=>string(6) "Class1"
																						 }
											 ["multiplicity"]=>array(2) {
																							[0]=>string(4) "2..4"
																							[1]=>string(4) "1..*"
																						}
									     ["roles"]=>array(2) {
																							[0]=>string(1) "e"
																							[1]=>string(1) "c"
																					}
											 ["type"]=>string(11) "association"
										}
	}

	@param $name {string} "R1"
	@param $classes {array} ["Student", "Class1"]
	@param $multiplicity {array} ["2..4","1..*"]
	@param $roles ["e","c"]
	**/
	function insert_relationship($classes, $name, $cardinalities, $roles){
		$array_rel = [];
		$array_rel = ["name" => $name,
									"classes" => $classes,
									"multiplicity" => $cardinalities,
									"roles" => $roles,
									"type" => "association"];
		array_push($this->content["links"],$array_rel);
	}
}



?>
