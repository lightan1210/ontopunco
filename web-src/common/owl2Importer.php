<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   owl2Importer.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom;

load("translator.php", "../wicom/translator/");
load("decoder.php", "../wicom/translator/");
load("crowd_uml.php", "../wicom/translator/strategies/");
load("berardistrat.php", "../wicom/translator/strategies/");
load("owllinkbuilder.php", "../wicom/translator/builders/");
load("owlbuilder.php", "../wicom/translator/builders/");
load("umljsonbuilder.php", "../wicom/translator/builders/");

load("runner.php", "../wicom/reasoner/");
load("racerconnector.php", "../wicom/reasoner/");
load("koncludeconnector.php", "../wicom/reasoner/");

load("ansanalizer.php", "../wicom/translator/strategies/qapackages/answeranalizers/");

use Wicom\Translator\Translator;
use Wicom\Translator\Decoder;
use Wicom\Translator\Strategies\UMLcrowd;
use Wicom\Translator\Strategies\Berardi;
use Wicom\Translator\Builders\OWLlinkBuilder;
use Wicom\Translator\Builders\OWLBuilder;
use Wicom\Translator\Builders\UMLJSONBuilder;

use Wicom\Reasoner\Runner;
use Wicom\Reasoner\RacerConnector;
use Wicom\Reasoner\KoncludeConnector;

use Wicom\Translator\Strategies\QAPackages\AnswerAnalizers\AnsAnalizer;
use Wicom\Translator\Strategies\QAPackages\QueriesGenerators\QueriesGenerator;

use SimpleXMLElement;
use SimpleXMLIterator;
use DOMDocument;
use XMLReader;
use XMLWriter;

class OWL2Importer extends Wicom{

    function __construct(){
      parent::__construct();
    }

    /**
    Put owl tags on owl 2 tags to convert it into owllink
    @param $owl2 A OWL 2 String
    @return An Array containing OWLlink elements from the input OWL 2
    */
    function owl2to_owllinkNaive($owl2){

      $str = [];
      $str2 = [];
      $prefixes = [];
      $pre = [];
      $owllink = [];
      $ontologyIRI = [];

      //Put XML elements into an array of string

      $owl_xml = new SimpleXMLIterator($owl2);
      $owl_xml->rewind();

      $header = $owl_xml->attributes();
      $ontologyIRI = ["prefix" => "", "value" => $header["ontologyIRI"]->__toString()];

      foreach ($owl_xml->children() as $child){

        if ($child->getName() != "Prefix"){
          array_push($str,$child->asXML());
        }
        elseif ($child->getName() == "Prefix"){
          $attr = $child->attributes();
          array_push($prefixes, ["prefix" => $attr[0]->__toString(), "value" => $attr[1]->__toString()]);
        }
      }

      //Replace OWL primitives adding OWL prefix for OWLlink document
      foreach ($str as $elem){
        $new = preg_replace("/\bSubClassOf\b/","owl:SubClassOf", $elem);
        $new2 = preg_replace("/\bClass\b/","owl:Class", $new);
        $new3 = preg_replace("/\bObjectProperty\b/","owl:ObjectProperty", $new2);
        $new4 = preg_replace("/\bObjectSomeValuesFrom\b/","owl:ObjectSomeValuesFrom", $new3);
        $new5 = preg_replace("/\bObjectMaxCardinality\b/","owl:ObjectMaxCardinality", $new4);
        $new6 = preg_replace("/\bObjectMinCardinality\b/","owl:ObjectMinCardinality", $new5);
        $new7 = preg_replace("/\bObjectInverseOf\b/","owl:ObjectInverseOf", $new6);
        $new8 = preg_replace("/\bEquivalentClasses\b/","owl:EquivalentClasses", $new7);
        $new9 = preg_replace("/\bObjectIntersectionOf\b/","owl:ObjectIntersectionOf", $new8);
        $new10 = preg_replace("/\bDisjointClasses\b/","owl:DisjointClasses", $new9);
        $new11 = preg_replace("/\bObjectPropertyDomain\b/","owl:ObjectPropertyDomain", $new10);
        $new12 = preg_replace("/\bObjectPropertyRange\b/","owl:ObjectPropertyRange", $new11);
        $new13 = preg_replace("/\bDataProperty\b/","owl:DataProperty", $new12);
        $new14 = preg_replace("/\bDataPropertyDomain\b/","owl:DataPropertyDomain", $new13);
        $new15 = preg_replace("/\bDataPropertyRange\b/","owl:DataPropertyRange", $new14);
//        $new16 = preg_replace("/#/","", $new15);
        array_push($str2,$new15);
      }
      array_push($owllink, ["ontologyIRI" => $ontologyIRI, "pre" => $prefixes, "owllink" =>$str2]);

      return $owllink;

    }

    /**
       Import an OWL 2 ontology and returns an approximated UML diagram.
       Currently supports importing of Classes, Subclasses, Object Properties,
       Cardinalities 0N, 1N, 01, 11.

       @todo Data Properties

       @param $owl2 A String with an OWL 2 ontology.
       @param $reasoner A String with the reasoner name for reasoning over the ontology being imported.
       We support two: Konclude and Racer.

       @return an UML JSON encoding a graphical diagram to be imported in crowd.
     */

    function owl2importer($owl2, $reasoner = 'Racer'){
        //Extracting IRIs and Prefixes from OWL 2 Ontology and writting OWLlink elements
        $owllink_a = $this->owl2to_owllinkNaive($owl2);

        $prefix = $owllink_a[0]["pre"];
        $owllink = $owllink_a[0]["owllink"];
        $ontologyIRI = $owllink_a[0]["ontologyIRI"];

  //      echo "cual esl la IRI=?"; var_dump($ontologyIRI);

        // Decoding OWL2 and generating UML JSON with extended IRIs. OK.
        $deco = new Decoder(new UMLcrowd(), new UMLJSONBuilder());

        $json_str = $deco->to_json($owl2, $ontologyIRI, $prefix);

    //    echo "JSON desde del DECODER"; var_dump($json_str);

        $encoding = new UMLcrowd();
        // Translating again the extracted ontology to OWL2 using UML crowd strategy
        // Merge owl2 and owllink into a new owllink document
        $trans = new Translator($encoding, new OWLlinkBuilder());

        // json_str es el json extraido del owl 2 (solo clases, atributos y relaciones con 0..N)
        // owllink es el owllink generado a partir del owl 2 de entrada.
        $owllink_str = $trans->importedto_owllink($json_str, $ontologyIRI, $prefix, $owllink);

  //      echo "OWLLink desp de translator"; var_dump($owllink_str);

        $reasonerconn = null;
        switch($reasoner){
          case "Konclude" :
              $reasonerconn = new KoncludeConnector();
              break;
          case "Racer" :
              $reasonerconn = new RacerConnector();
              break;
          default: console.log($reasonerconn); die("Reasoner Not Found!");
        }

        // Reasoning on the extracted ontology
        $runner = new Runner($reasonerconn);
        $runner->run($owllink_str);

        $reasoner_answer = $runner->get_last_answer();

  //      echo "REASONER ANSWER"; var_dump($reasoner_answer); var_dump($ontologyIRI);
  //      echo "ANSWER";
        $answer = $encoding->analize_answer($owllink_str, $reasoner_answer, $owl2);

  //      echo "ANSWER"; var_dump($answer);

        $owl2decode = $answer->get_new_owl2()->to_string();

//        var_dump($owl2decode);

        // Decoding last OWL2 and generating UML JSON
        $owldeco_end = new Decoder(new UMLcrowd(), new UMLJSONBuilder());
        $json_end = $owldeco_end->to_json($owl2decode, $ontologyIRI, $prefix);

        return $json_end;

    }
}

?>
