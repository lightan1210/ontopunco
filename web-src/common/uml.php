<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   wicom.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom;

load("translator.php", "../wicom/translator/");
load("crowd_uml.php", "../wicom/translator/strategies/");
load("berardistrat.php", "../wicom/translator/strategies/");
load("owllinkbuilder.php", "../wicom/translator/builders/");
load("owlbuilder.php", "../wicom/translator/builders/");
load("umljsonbuilder.php", "../wicom/translator/builders/");
load("decoder.php", "../wicom/translator/");
load("config.php", "../config/");

load("obda.php", "../wicom/obda/");


load("runner.php", "../wicom/reasoner/");
load("racerconnector.php", "../wicom/reasoner/");
load("koncludeconnector.php", "../wicom/reasoner/");
//load("automapconnector.php", "../wicom/reasoner/");
//load("ontopconnector.php", "../wicom/reasoner/");
load("automapmapper.php","../wicom/obda/");
load("ontopframework.php","../wicom/obda/");


load("ansanalizer.php", "../wicom/translator/strategies/qapackages/answeranalizers/");

use Wicom\Translator\Translator;
use Wicom\Translator\Strategies\UMLcrowd;
use Wicom\Translator\Strategies\Berardi;
use Wicom\Translator\Builders\OWLlinkBuilder;
use Wicom\Translator\Builders\OWLBuilder;
use Wicom\Translator\Builders\UMLJSONBuilder;
use Wicom\Translator\Decoder;

use Wicom\Reasoner\Runner;
use Wicom\Reasoner\RacerConnector;
use Wicom\Reasoner\KoncludeConnector;
use Wicom\OBDA\Ontop;
use Wicom\OBDA\Automap;
//use Wicom\Reasoner\AutomapConnector;
//use Wicom\Reasoner\OntopConnector;

use Wicom\OBDA\OBDAInitializer;

use Wicom\Translator\Strategies\QAPackages\AnswerAnalizers\AnsAnalizer;
use Wicom\Translator\Strategies\QAPackages\QueriesGenerators\QueriesGenerator;

class UML_Wicom extends Wicom{

    function __construct(){
      parent::__construct();
    }

    /**
       Check the diagram represented in JSON format for full reasoning.

       @param $json_str A String with the diagram in JSON format.
       @param $strategy A String representing an specific Description Logic encoding
       @param $reasoner A String with the reasoner name. We support two: Konclude and Racer.

       @return Wicom\Translator\Strategies\QAPackages\AnswerAnalizers\Answer an answer object.
     */
    function full_reasoning($json_str, $strategy = 'crowd', $reasoner = 'Racer'){

        $encoding = null;
        switch($strategy){
          case "berardi" :
              $encoding = new Berardi();
              break;
          case "crowd" :
              $encoding = new UMLcrowd();
              break;
          default: console.log($strategy); die("Invalid Encoding");
        }

        $trans = new Translator($encoding, new OWLlinkBuilder());

        $owllink_str = $trans->to_owllink($json_str);

        $reasonerconn = null;
        switch($reasoner){
          case "Konclude" :
              $reasonerconn = new KoncludeConnector();
              break;
          case "Racer" :
              $reasonerconn = new RacerConnector();
              break;
          default: console.log($reasonerconn); die("Reasoner Not Found!");
        }

        $runner = new Runner($reasonerconn);
        $runner->run($owllink_str);
        $reasoner_answer = $runner->get_last_answer();

        $owl2_trans = new Translator($encoding, new OWLBuilder());
        $encoding->change_min_maxTo_false();
        $owl2_str = $owl2_trans->to_owl2($json_str);

//        var_dump($owl2_str);



        $answer = $encoding->analize_answer($owllink_str, $reasoner_answer, $owl2_str);

        $owldeco_end = new Decoder(new UMLcrowd(), new UMLJSONBuilder());
        $json_end = $owldeco_end->to_json($answer->get_new_owl2()->to_string(), [], []);

//        var_dump($json_end);

		return $answer;

    }

    /*function automapping($json_str){

        $trans = new Translator(new UMLcrowd(), new OWLBuilder());
        $owl2_str = $trans->to_owl2($json_str);

        $runner = new Runner(new AutomapConnector());
        $runner->run($owl2_str);
        $automap_answer = $runner->get_last_answer();

        return $automap_answer;
    }*/

    function getMappings($json_str, $mapToolStr, $dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema){


        switch($mapToolStr){
          
          case "Automap" :
              $mapper = new Automap();
              break;
         
          default: console.log($reasonerconn); die("Reasoner Not Found!");
        }


       


        $obdaInitializer = new OBDAInitializer($mapper, $obdaFrw, $json_str, $queries, $dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema);

        $answer = $obdaInitializer->allTogether(); 

        //var_dump($answer);

        return $answer;
    }




      function obda($mapToolStr, $obdaFrwStr, $json_str, $mappings, $queries, $dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema, $temporalID){

          switch($mapToolStr){
            
            case "Automap" :
                $mapper = new Automap();
                break;

            case null:
                $mapper = null;
                break;

            default: console.log($reasonerconn); die("Mapping tool not Found!");
          }
        

        switch($obdaFrwStr){
          
          case "Ontop" :
              $obdaFrw = new Ontop();
              break;
         
          default: console.log($reasonerconn); die("OBDA framework not found!");
        }


        $obdaInitializer = new OBDAInitializer($mapper, $obdaFrw, $json_str, $mappings, $queries, $dbIp, $dbUsername, $dbPassword, $dbDriver, $dbName, $dbSchema, $temporalID);

        $answer = $obdaInitializer->obda(); 

        /*if ($mapToolStr != null){
          $answer = $obdaInitializer->allTogether(); 
        }else{
          $answer = $obdaInitializer->obda(); 
        }*/


        return $answer;
    }




    




}

?>
